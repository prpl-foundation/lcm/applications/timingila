/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <sys/time.h>
#include <sys/resource.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <inttypes.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include <timingila/timingila_defines.h>

#include "timingila_priv.h"
#include "test_load_plugin.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_definitions = "../../odl-ambiorix/timingila_definition.odl";

static amxm_module_t dummy;

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);
amxm_module_t* __wrap_amxm_so_get_module(const amxm_shared_object_t* const shared_object,
                                         const char* const module_name);
int __wrap_amxm_so_close(amxm_shared_object_t** so);

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        UNUSED const char* shared_object_name,
                        UNUSED const char* const path_to_so) {
    int rv = (int) mock();
    if(rv == 0) {
        *so = (amxm_shared_object_t*) calloc(1, sizeof(amxm_shared_object_t));
    } else {
        *so = NULL;
    }

    return rv;
}

amxm_module_t* __wrap_amxm_so_get_module(UNUSED const amxm_shared_object_t* const shared_object,
                                         UNUSED const char* const module_name) {
    amxm_module_t* mod = mock_ptr_type(amxm_module_t*);
    return mod;
}

int __wrap_amxm_so_close(amxm_shared_object_t** so) {
    if(*so != NULL) {
        free(*so);
        *so = NULL;
    }

    return 0;
}

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_timingila_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "loadPluginContainer", AMXO_FUNC(_Timingila_loadPluginContainer)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "loadPluginPackager", AMXO_FUNC(_Timingila_loadPluginPackager)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "rmAfterUninstall", AMXO_FUNC(_Timingila_rmAfterUninstall)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "lateInit", AMXO_FUNC(_Timingila_lateInit)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_definitions, root_obj), 0);

    _timingila_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_timingila_teardown(UNUSED void** state) {
    _timingila_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_timingila_rpc_fails_when_no_modules_loaded(UNUSED void** state) {
    amxd_object_t* timingila = amxd_dm_findf(&dm, "Timingila");
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(timingila);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_not_equal(amxd_object_invoke_function(timingila, "rmAfterUninstall", &args, &ret), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_timingila_can_load_plugin_container(UNUSED void** state) {
    amxd_object_t* timingila = amxd_dm_findf(&dm, "Timingila");
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(timingila);

    will_return(__wrap_amxm_so_open, -1);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/timingila/my_mod.so");
    assert_int_not_equal(amxd_object_invoke_function(timingila, "loadPluginContainer", &args, &ret), amxd_status_ok);

    will_return(__wrap_amxm_so_open, 0);
    will_return(__wrap_amxm_so_get_module, NULL);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/timingila/my_mod.so");
    assert_int_not_equal(amxd_object_invoke_function(timingila, "loadPluginContainer", &args, &ret), amxd_status_ok);

    will_return(__wrap_amxm_so_open, 0);
    will_return(__wrap_amxm_so_get_module, &dummy);
    will_return(__wrap_amxm_so_execute_function, 0);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/timingila/my_mod.so");
    assert_int_equal(amxd_object_invoke_function(timingila, "loadPluginContainer", &args, &ret), amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/timingila/my_mod_other.so");
    assert_int_not_equal(amxd_object_invoke_function(timingila, "loadPluginContainer", &args, &ret), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_timingila_can_load_plugin_packager(UNUSED void** state) {
    amxd_object_t* timingila = amxd_dm_findf(&dm, "Timingila");
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(timingila);

    will_return(__wrap_amxm_so_open, -1);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/timingila/my_mod.so");
    assert_int_not_equal(amxd_object_invoke_function(timingila, "loadPluginPackager", &args, &ret), amxd_status_ok);

    will_return(__wrap_amxm_so_open, 0);
    will_return(__wrap_amxm_so_get_module, NULL);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/timingila/my_mod.so");
    assert_int_not_equal(amxd_object_invoke_function(timingila, "loadPluginPackager", &args, &ret), amxd_status_ok);

    will_return(__wrap_amxm_so_open, 0);
    will_return(__wrap_amxm_so_get_module, &dummy);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/timingila/my_mod.so");
    assert_int_equal(amxd_object_invoke_function(timingila, "loadPluginPackager", &args, &ret), amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "plugin", "/usr/lib/timingila/my_mod_other.so");
    assert_int_not_equal(amxd_object_invoke_function(timingila, "loadPluginPackager", &args, &ret), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}