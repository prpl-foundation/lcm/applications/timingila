/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <timingila/timingila_defines.h>

#include "wrap_amxm.h"

#define UNUSED __attribute__((unused))

static int mod_install_du(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    printf("Args = %s, %s, %s\n", GET_CHAR(args, SM_DM_DU_URL), GET_CHAR(args, SM_DM_DU_UUID), GET_CHAR(args, SM_DM_DU_EE_REF));
    return 0;
}
static int mod_add_exec_env(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    printf("Args = %s, %s, %s, %s, %d, %d, %d\n",
           GET_CHAR(args, SM_DM_EE_NAME), GET_CHAR(args, SM_DM_EE_VENDOR), GET_CHAR(args, SM_DM_EE_VERSION), GET_CHAR(args, SM_DM_EE_PARENT_EE),
           GET_INT32(args, SM_DM_EE_ALLOCMEM), GET_INT32(args, SM_DM_EE_ALLOCDISKSPACE), GET_INT32(args, SM_DM_EE_ALLOCCPUPERCENT));
    return 0;
}
static int mod_update_du(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    printf("Args = %s, %s, %s, %s\n", GET_CHAR(args, SM_DM_DU_URL), GET_CHAR(args, SM_DM_DU_UUID), GET_CHAR(args, SM_DM_DU_USERNAME), GET_CHAR(args, SM_DM_DU_PASSWORD));
    return 0;
}

static int mod_uninstall_du(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    printf("Args = %s, %s\n", GET_CHAR(args, SM_DM_DU_UUID), GET_CHAR(args, SM_DM_DU_EE_REF));
    return 0;
}

static int mod_restart_ee(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    printf("Args = %s\n", GET_CHAR(args, SM_DM_EE_RESTART_REASON));
    return 0;
}

static int mod_modify_constraints_ee(UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    printf("Args = \n");
    return 0;
}
static int mod_delete_ee(UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    printf("Args = \n");
    return 0;
}
static int mod_setrequestedstate_eu(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    printf("Args = %s\n", GET_CHAR(args, SM_DM_EU_REQSTATE));
    return 0;
}
static int mod_eu_nodifynetworkconfig(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxc_var_dump(args, STDOUT_FILENO);
    return 0;
}

static int mod_eu_validateinstallargs(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxc_var_dump(args, STDOUT_FILENO);
    return 0;
}

static int mod_eu_validateupdateargs(amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxc_var_dump(args, STDOUT_FILENO);
    return 0;
}

int __wrap_amxm_so_execute_function(UNUSED amxm_shared_object_t* const so,
                                    const char* const module_name,
                                    const char* const func_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    printf("Module_name %s fname %s\n", module_name, func_name);
    if(strcmp(func_name, TIMINGILA_PACKAGER_INSTALLDU) == 0) {
        return mod_install_du(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_PACKAGER_UPDATEDU) == 0) {
        return mod_update_du(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_PACKAGER_UNINSTALLDU) == 0) {
        return mod_uninstall_du(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_CONTAINER_EE_RESTART) == 0) {
        return mod_restart_ee(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_CONTAINER_EE_ADD) == 0) {
        return mod_add_exec_env(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_CONTAINER_EE_DELETE) == 0) {
        return mod_delete_ee(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_CONTAINER_EE_MODIFYCONSTRAINTS) == 0) {
        return mod_modify_constraints_ee(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_CONTAINER_EU_SETREQUESTEDSTATE) == 0) {
        return mod_setrequestedstate_eu(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_CONTAINER_EU_MODIFYNETWORKCONFIG) == 0) {
        return mod_eu_nodifynetworkconfig(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_CONTAINER_EU_VALIDATEINSTALLARGS) == 0) {
        return mod_eu_validateinstallargs(args, ret);
    }
    if(strcmp(func_name, TIMINGILA_CONTAINER_EU_VALIDATEUPDATEARGS) == 0) {
        return mod_eu_validateupdateargs(args, ret);
    }

    int rv = (int) mock();

    return rv;
}
