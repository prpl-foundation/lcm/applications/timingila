/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <sys/time.h>
#include <sys/resource.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <inttypes.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include <timingila/timingila_defines.h>
#include <debug/sahtrace.h>
#include "timingila_priv.h"
#include "softwaremodules_priv.h"
#include "test_autorestart.h"

#include "test_setup.h"

#define UNUSED __attribute__((unused))

#define TEST_UUID "00000000-0971-52b1-b70c-47ab6ce48227"

int test_autorestart_setup(UNUSED void** state) {
    return test_softwaremodules_setup(state);
}

int test_autorestart_teardown(UNUSED void** state) {
    return test_softwaremodules_teardown(state);
}

static int auto_param_value(amxc_llist_t* auto_res, bool enable, int32_t max_retry_count, int32_t min_interval, int32_t max_interval,
                            int32_t retry_int_mul, int32_t reset_period) {
    int res = 0;
    amxc_var_t* auto_value = NULL;

    when_failed(amxc_var_new(&auto_value), exit);
    when_failed(amxc_var_set_type(auto_value, AMXC_VAR_ID_HTABLE), exit);

    amxc_var_add_new_key_int32_t(auto_value, SM_DM_EU_AUTORESTART_ENABLE, enable);
    amxc_var_add_new_key_int32_t(auto_value, SM_DM_EU_AUTORESTART_MAX_RETRYCOUNT, max_retry_count);
    amxc_var_add_new_key_int32_t(auto_value, SM_DM_EU_AUTORESTART_MIN_INT, min_interval);
    amxc_var_add_new_key_int32_t(auto_value, SM_DM_EU_AUTORESTART_MAX_INT, max_interval);
    amxc_var_add_new_key_int32_t(auto_value, SM_DM_EU_AUTORESTART_RETRY_INT, retry_int_mul);
    amxc_var_add_new_key_int32_t(auto_value, SM_DM_EU_AUTORESTART_RESET, reset_period);

    when_failed(amxc_llist_append(auto_res, &(auto_value)->lit), exit);
exit:
    return res;
}

void test_install_du(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_llist_t* auto_res = NULL;


    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_URL, "url");
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_UUID, "10000000-0971-52b1-b70c-47ab6ce48227");
    amxc_var_add_key(cstring_t, &args, SM_DM_DU_EE_REF, "SoftwareModules.ExecEnv.1.");
    assert_int_equal(amxc_llist_new(&auto_res), 0);
    auto_param_value(auto_res, true, 10, 60, 86400, 2000, 200);
    amxc_var_add_new_key_amxc_llist_t(&args, SM_DM_EU_AUTORESTART, auto_res);
    assert_int_equal(amxd_object_invoke_function(dm_obj, "InstallDU", &args, &ret), amxd_status_deferred);
    amxc_llist_delete(&auto_res, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

}

void test_uninstall_du(UNUSED void** state) {
    amxd_object_t* dm_obj = amxd_dm_findf(test_get_dm(), SOFTWAREMODULES_DM_DU ".cpe-" TEST_UUID);
    amxc_var_t args;
    amxc_var_t ret;


    assert_non_null(dm_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(dm_obj, "Uninstall", &args, &ret), amxd_status_deferred);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

}
