MACHINE = $(shell $(CC) -dumpmachine)
SHELL = /bin/bash

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv)
COMMON_SRCDIR = $(realpath ../common)
INCDIR += $(realpath ../common_includes)

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/timingila*.c)
SOURCES += $(wildcard $(COMMON_SRCDIR)/*.c)

CFLAGS += -Wall -Wextra -Wno-attributes \
          --std=gnu99 -g3 -Wmissing-declarations \
		  -Wno-unused-but-set-variable \
		  -Wno-unused-variable \
		  $(addprefix -I ,$(INCDIR)) \
		  -fprofile-arcs -ftest-coverage \
		  -fkeep-inline-functions -fkeep-static-functions \
		  $(shell pkg-config --cflags cmocka)
LDFLAGS += -fprofile-arcs -ftest-coverage \
           -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxd -lamxm -lamxo -lamxp -lsahtrace -luuid -llcm
