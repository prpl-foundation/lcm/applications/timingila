/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"

#include "dmmngr_priv.h"
#include "timingila_priv.h"

#define ME TIMINGILA_MOD_DM_MNGR "-du"

static int timingila_dm_mngr_update_du(UNUSED const char* function_name,
                                       amxc_var_t* args,
                                       amxc_var_t* ret) {
    int retval = -1;
    amxd_object_t* dm_obj = NULL;
    amxd_dm_t* dm = timingila_get_dm();

    const char* eeref = GET_CHAR(args, SM_DM_DU_EE_REF);
    if(eeref) {
        if(string_starts_with_device_prefix(eeref) == false) {
            char* neweeref = NULL;
            if(string_starts_with_sm_prefix(eeref) == true) {
                size_t len_new_eeref = strlen(eeref) + sizeof(DEVICE_DM_PREFIX);
                neweeref = malloc(len_new_eeref);
                if(sprintf(neweeref, DEVICE_DM_PREFIX "%s", eeref) != (int) len_new_eeref) {
                    SAH_TRACEZ_ERROR(ME, "Something went wrong with adding " DEVICE_DM_PREFIX " to the eeref");
                }
            } else {
                amxd_object_t* ee_obj = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EE ".[" SM_DM_EE_NAME " == '%s'].", eeref);
                if(ee_obj) {
                    uint32_t index_ee = amxd_object_get_index(ee_obj);
                    if(index_ee) {
                        neweeref = malloc(sizeof(DEVICE_DM_PREFIX) + sizeof(SOFTWAREMODULES_DM_EE ".") + MAX_STR_LEN_U32 + sizeof("."));
                        if(sprintf(neweeref, DEVICE_DM_PREFIX SOFTWAREMODULES_DM_EE ".%" PRIu32 ".", index_ee) < ((int) sizeof(SOFTWAREMODULES_DM_EE "."))) {
                            SAH_TRACEZ_ERROR(ME, "Something went wrong with generating the eeref from EE " SM_DM_EE_NAME);
                        }
                    } else {
                        SAH_TRACEZ_ERROR(ME, "Cannot get index of EE with " SM_DM_EE_NAME " '%s'", eeref);
                    }
                } else {
                    SAH_TRACEZ_ERROR(ME, "Cannot find: EE with " SM_DM_EE_NAME " '%s'", eeref);
                }
            }

            if(neweeref) {
                amxc_var_t* varptr = amxc_var_take_key(args, SM_DM_DU_EE_REF);
                if(varptr) {
                    amxc_var_clean(varptr);
                    amxc_var_delete(&varptr);
                }
                amxc_var_add_key(cstring_t, args, SM_DM_DU_EE_REF, neweeref);
                free(neweeref);
            }
        }
    }

    // Get Unique Keys
    dm_obj = get_du_obj_from_keys(dm, args);
    if(dm_obj) {
        // update instance
        SAH_TRACEZ_INFO(ME, "Update DeploymentUnit instance");
        retval = timingila_dm_mngr_update(dm_obj, args);
    } else {
        // add new instance
        amxd_object_t* deployment_unit = amxd_dm_findf(dm, SOFTWAREMODULES_DM_DU);
        if(deployment_unit == NULL) {
            SAH_TRACEZ_ERROR(ME, "Cannot get" SOFTWAREMODULES_DM_DU);
            goto exit;
        }

        SAH_TRACEZ_INFO(ME, "Create DeploymentUnit instance");
        retval = timingila_dm_mngr_create(deployment_unit, args, ret);
    }

exit:
    return retval;
}

static int timingila_dm_mngr_remove_du(UNUSED const char* function_name,
                                       amxc_var_t* args,
                                       UNUSED amxc_var_t* ret) {
    int retval = -1;
    char* du_version = NULL;
    const char* img_version = NULL;
    amxd_object_t* dm_obj = NULL;
    amxd_dm_t* dm = timingila_get_dm();

    dm_obj = get_du_obj_from_keys(dm, args);
    if(dm_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot find DeploymentUnit instance");
        goto exit;
    }

    img_version = GET_CHAR(args, SM_DM_DU_VERSION);
    du_version = amxd_object_get_value(cstring_t, dm_obj, SM_DM_DU_VERSION, NULL);
    if(!img_version) {
        SAH_TRACEZ_ERROR(ME, SM_DM_DU_VERSION " is not defined in arguments");
        goto exit;
    }
    if(strcmp(du_version, img_version) != 0) {
        // If the version of the DU doesn't match the version of the image being removed,
        // do not remove the DU
        retval = 0;
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Remove DeploymentUnit instance");
    retval = timingila_dm_mngr_remove(&dm_obj);
exit:
    free(du_version);
    return retval;
}

int timingila_dm_mngr_du_init(void) {
    timingila_dm_mngr_add_function(TIMINGILA_MOD_DM_MNGR_UPDATE_DU, timingila_dm_mngr_update_du);
    timingila_dm_mngr_add_function(TIMINGILA_MOD_DM_MNGR_REMOVE_DU, timingila_dm_mngr_remove_du);
    return 0;
}
