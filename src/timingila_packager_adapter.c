/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"

#include "timingila_priv.h"
#include "timingila_packager_adapter.h"

#define ME "packager_adapter"

static amxm_shared_object_t* packager_adapter = NULL;

amxm_shared_object_t* get_packager_adapter(void) {
    return packager_adapter;
}

bool open_packager_adapter(const char* plugin_so) {
    bool ret = false;

    if(packager_adapter) {
        SAH_TRACEZ_ERROR(ME, "Packager adapter already set: %p", packager_adapter);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Load plugin (%s) as packager", plugin_so);
    if(amxm_so_open(&packager_adapter, "packager", plugin_so) != 0) {
        SAH_TRACEZ_ERROR(ME, "Can not load plugin %s", plugin_so);
        goto exit;
    }

    if(amxm_so_get_module(packager_adapter, MOD_TIMINGILA_PACKAGER) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Library does not provide %s", MOD_TIMINGILA_PACKAGER);
        amxm_so_close(&packager_adapter);
        goto exit;
    }

    // set datamodel entry
    {
        amxd_dm_t* dm = timingila_get_dm();
        amxd_object_t* timingila = amxd_dm_get_object(dm, TIMINGILA_DM);
        amxc_var_t dmchange;
        amxc_var_init(&dmchange);
        amxc_var_set_type(&dmchange, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &dmchange, "PackagerPluginPath", plugin_so);
        timingila_dm_mngr_update(timingila, &dmchange);
        amxc_var_clean(&dmchange);
    }

    ret = true;
exit:
    return ret;
}

bool close_packager_adapter(void) {
    return amxm_so_close(&packager_adapter);
}

bool packager_RemoveAfterUninstall(amxc_var_t* args) {
    int error = 0;

    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(packager_adapter,
                                     MOD_TIMINGILA_PACKAGER,
                                     TIMINGILA_PACKAGER_RMAFTERUNINSTALL,
                                     args,
                                     &ret);
    amxc_var_clean(&ret);

    if(error) {
        return false;
    } else {
        {
            bool blenable = GET_BOOL(args, "enable");
            amxd_dm_t* dm = timingila_get_dm();
            amxd_object_t* timingila = amxd_dm_get_object(dm, TIMINGILA_DM);
            amxc_var_t dmchange;
            amxc_var_init(&dmchange);
            amxc_var_set_type(&dmchange, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(bool, &dmchange, "RmAfterUninstall", blenable);
            timingila_dm_mngr_update(timingila, &dmchange);
            amxc_var_clean(&dmchange);
        }
        return true;
    }
}