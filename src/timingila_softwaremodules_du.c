/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"
#include "lcm/lcm_dump_rpc.h"

#include "softwaremodules_priv.h"
#include "timingila_priv.h"

#define ME "softwaremodules_du"

static inline void dustatechange_event_error(const char* uuid, const char* operation, uint64_t faulttype, const char* faulttypestr, const char* executionenvref) {
    amxc_var_t* notif_data = NULL;

    SAH_TRACEZ_WARNING(ME, "Event error: UUID: %s, Operation: %s, FaultType: %" PRIu64 ", Fault: %s", uuid, operation, faulttype, faulttypestr);

    amxc_var_new(&notif_data);
    amxc_var_set_type(notif_data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_UUID, uuid);
    amxc_var_add_key(cstring_t, notif_data, SM_DM_DU_STATUS, SM_DM_EVENT_DUSTATECHANGE_FAILED);
    amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_OPERATION, operation);
    amxc_var_add_key(uint32_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTCODE, faulttype);
    amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING, faulttypestr);
    amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_EE_REF, executionenvref);
    amxc_var_add_key(bool, notif_data, SM_DM_EVENT_DUSTATECHANGE_RESOLVED, false);

    timingila_dm_mngr_event_dustatechange(NULL, notif_data, NULL);

    amxc_var_delete(&notif_data);
}

static inline amxd_object_t* get_du(const char* duid) {
    amxd_object_t* du = NULL;
    amxd_dm_t* dm = timingila_get_dm();
    du = amxd_dm_findf(dm, SOFTWAREMODULES_DM_DU ".[" SM_DM_DU_DUID " == '%s'].", duid);
    return du;
}

inline amxd_status_t check_uuid(const char* uuid, const char* operation, amxc_var_t* args, const char* executionenvref) {
    // a uuid should have this pattern, with
    // x = 0-9|a-f|A-Z
    // N = 8,9,a,b,A,B
    // other characters should be litterally equal
    static const char* pattern = "xxxxxxxx-xxxx-5xxx-Nxxx-xxxxxxxxxxxx";
    if(uuid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Empty " SM_DM_DU_UUID);
        goto error;
    }
    if(strlen(uuid) != strlen(pattern)) {
        SAH_TRACEZ_ERROR(ME, "UUID has wrong size. [%s] should have pattern %s", uuid, pattern);
        goto error;
    }
    for(size_t pos = 0; pos < strlen(pattern); ++pos) {
        bool err = false;
        if((pattern[pos] == 'x')) {
            if(!isxdigit(uuid[pos])) {
                err = true;
            }
        } else if(pattern[pos] == 'N') {
            if(!((uuid[pos] == '8') || (uuid[pos] == '9') ||
                 (uuid[pos] == 'a') || (uuid[pos] == 'b') ||
                 (uuid[pos] == 'A') || (uuid[pos] == 'B'))) {
                err = true;
            }
        } else if(pattern[pos] != uuid[pos]) {
            err = true;
        }
        if(err) {
            SAH_TRACEZ_ERROR(ME, "UUID is wrong on position %ld. [%s] should have pattern %s", pos, uuid, pattern);
            goto error;
        }
    }
    return amxd_status_ok;
error:
    amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_UUID_FORMAT_NON_COMPLIANT);
    amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_UUID_FORMAT_NON_COMPLIANT_STR);

    dustatechange_event_error(uuid ? uuid : "", operation, ERR_UUID_FORMAT_NON_COMPLIANT, ERR_UUID_FORMAT_NON_COMPLIANT_STR, executionenvref);
    return amxd_status_invalid_arg;

}

static inline amxd_status_t check_executionenvref(const char* executionenvref, const char* uuid, const char* operation, amxc_var_t* args) {
    if((executionenvref == NULL) || (executionenvref[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "Empty or missing " SM_DM_DU_EE_REF);

        amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_MISSING_EXECUTION_ENVIRONMENT);
        amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_MISSING_EXECUTION_ENVIRONMENT_STR);

        dustatechange_event_error(uuid ? uuid : "", operation, ERR_MISSING_EXECUTION_ENVIRONMENT, ERR_MISSING_EXECUTION_ENVIRONMENT_STR, "");
        return amxd_status_invalid_arg;
    }
    return amxd_status_ok;
}

static inline amxd_status_t check_ee_disabled(const char* executionenvref, const char* uuid, const char* operation, amxc_var_t* args, const char* oldexecutionenvref) {
    amxd_dm_t* dm = timingila_get_dm();
    amxc_var_t ee_obj_params;
    const char* ee_status = NULL;
    int compareret = 0;
    amxd_object_t* ee_obj = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EE ".[" SM_DM_EE_NAME " == '%s'].", executionenvref);
    if(ee_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot find EE '%s'", executionenvref);

        amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_UNKNOWN_EXECUTION_ENVIRONMENT);
        amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_UNKNOWN_EXECUTION_ENVIRONMENT_STR);

        dustatechange_event_error(uuid, operation, ERR_UNKNOWN_EXECUTION_ENVIRONMENT, ERR_UNKNOWN_EXECUTION_ENVIRONMENT_STR, oldexecutionenvref);

        return amxd_status_invalid_arg;
    }

    amxc_var_init(&ee_obj_params);
    amxd_object_get_params(ee_obj, &ee_obj_params, amxd_dm_access_public);
    ee_status = GET_CHAR(&ee_obj_params, SM_DM_EE_STATUS);
    compareret = strcmp(ee_status, SM_DM_EE_STATUS_DISABLED);
    amxc_var_clean(&ee_obj_params);
    if(compareret == 0) {
        SAH_TRACEZ_ERROR(ME, "EE '%s' is not enabled, so we will return error", executionenvref);

        amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH);
        amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH_STR);

        dustatechange_event_error(uuid, operation, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH_STR, executionenvref);

        return amxd_status_invalid_arg;
    }
    return amxd_status_ok;
}
#ifdef TIMINGILA_DEBUG
static void print_optional_arg_cstring_t(amxc_var_t* args, const char* const key) {
    amxc_var_t* var = NULL;
    if(( var = amxc_var_get_key(args, key, AMXC_VAR_FLAG_DEFAULT)) != NULL) {
        SAH_TRACEZ_INFO(ME, "%s: %s", key, amxc_var_get_const_cstring_t(var));
    }
}

static void print_optional_arg_int32_t(amxc_var_t* args, const char* const key) {
    amxc_var_t* var = NULL;
    if(( var = amxc_var_get_key(args, key, AMXC_VAR_FLAG_DEFAULT)) != NULL) {
        SAH_TRACEZ_INFO(ME, "%s: %d", key, amxc_var_get_int32_t(var));
    }
}

static void print_optional_arg_bool(amxc_var_t* args, const char* const key) {
    amxc_var_t* var = NULL;
    if(( var = amxc_var_get_key(args, key, AMXC_VAR_FLAG_DEFAULT)) != NULL) {
        SAH_TRACEZ_INFO(ME, "%s: %s", key, amxc_var_get_bool(var) ? "true" : "false");
    }
}
#endif

// Handle path for SM_DM_DU_EE_REF
// We will give the name of the EE to the backends
static amxd_status_t handle_executionenvref_path(char** executionenvref, amxc_var_t* args, const char* operation, amxd_dm_t* dm, const char* uuid) {
    amxd_object_t* ee_obj = NULL;
    const char* eeref_parsed = (*executionenvref);
    for(unsigned int i = 0; (*executionenvref)[i] != '\0'; i++) {
        if((*executionenvref)[i] == '.') {
            amxc_var_t* varptr = NULL;
            SAH_TRACEZ_INFO(ME, SM_DM_DU_EE_REF " is a path: '%s'", eeref_parsed);
            if(string_starts_with_device_prefix(eeref_parsed)) {
                eeref_parsed = (const char*) (eeref_parsed + (sizeof(DEVICE_DM_PREFIX) - (size_t) 1));
            }

            if(string_starts_with_sm_prefix(eeref_parsed) == false) {
                SAH_TRACEZ_ERROR(ME, SM_DM_DU_EE_REF " (%s) doesnt contain '" SOFTWAREMODULES_DM ".'", eeref_parsed);
                amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH);
                amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH_STR);

                dustatechange_event_error(uuid, operation, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH_STR, *executionenvref);
                return amxd_status_invalid_arg;
            }

            ee_obj = amxd_dm_findf(dm, "%s", eeref_parsed);
            if(ee_obj) {
                free(*executionenvref);
                (*executionenvref) = amxd_object_get_value(cstring_t, ee_obj, SM_DM_EE_NAME, NULL);
            } else {
                SAH_TRACEZ_ERROR(ME, "Cannot resolve '%s'", eeref_parsed);
                amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH);
                amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH_STR);

                dustatechange_event_error(uuid, operation, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH, ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH_STR, *executionenvref);
                return amxd_status_invalid_arg;
            }

            varptr = amxc_var_take_key(args, SM_DM_DU_EE_REF);
            if(varptr) {
                amxc_var_clean(varptr);
                amxc_var_delete(&varptr);
            }
            amxc_var_add_key(cstring_t, args, SM_DM_DU_EE_REF, *executionenvref);
            break;
        }
    }

    return amxd_status_ok;
}

amxd_status_t _SoftwareModules_InstallDU(UNUSED amxd_object_t* object,
                                         amxd_function_t* func,
                                         amxc_var_t* args,
                                         amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, "Password");
    bool retb = false;
    amxd_dm_t* dm = timingila_get_dm();
    amxd_status_t status = amxd_status_unknown_error;
    amxd_status_t helpstatus = amxd_status_unknown_error;
    uint64_t callid;
    const char* uuid = GET_CHAR(args, SM_DM_DU_UUID);
    const char* oldexecutionenvref = GET_CHAR(args, SM_DM_DU_EE_REF);
    const char* operation = SM_DM_EVENT_DUSTATECHANGE_OPERATION_INSTALL;
    const char* url = GET_CHAR(args, SM_DM_DU_URL);
    char* executionenvref = NULL;
    char* duid = NULL;
    amxd_object_t* du_obj = NULL;
    executionenvref = strdup(oldexecutionenvref);

    if(check_uuid(uuid, operation, args, executionenvref)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }

    if(check_executionenvref(executionenvref, uuid, operation, args)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }

    helpstatus = handle_executionenvref_path(&executionenvref, args, operation, dm, uuid);
    if(helpstatus != amxd_status_ok) {
        status = helpstatus;
        goto exitfree;
    }

    duid = generate_duid(uuid, executionenvref);
    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot generate " SM_DM_DU_DUID);
        status = amxd_status_invalid_arg;
        goto exitfree;
    }

    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, duid);

#ifdef TIMINGILA_DEBUG
    print_optional_arg_cstring_t(args, SM_DM_DU_URL);
    print_optional_arg_cstring_t(args, SM_DM_DU_UUID);
    print_optional_arg_cstring_t(args, SM_DM_DU_USERNAME);
    print_optional_arg_cstring_t(args, SM_DM_DU_PASSWORD);
    print_optional_arg_cstring_t(args, SM_DM_DU_EE_REF);
    print_optional_arg_int32_t(args, SM_DM_DU_ALLOCCPUPERCENT);
    print_optional_arg_int32_t(args, SM_DM_DU_ALLOCMEM);
    print_optional_arg_int32_t(args, SM_DM_DU_ALLOCDISKSPACE);
    print_optional_arg_bool(args, SM_DM_DU_AUTOSTART);
    print_optional_arg_cstring_t(args, SM_DM_DU_DUID);
    print_optional_arg_cstring_t(args, SM_DM_DU_REQUIRED_ROLES);
    print_optional_arg_cstring_t(args, SM_DM_DU_OPTIONAL_ROLES);
#endif // TIMINGILA_DEBUG

    // check if DU already exists
    du_obj = get_du(duid);
    free(duid);
    duid = NULL;
    if(du_obj) {
        const char* du_url = amxd_object_get_value(cstring_t, du_obj, SM_DM_DU_URL, NULL);
        SAH_TRACEZ_ERROR(ME, "Found DU with DUID");
        if(strcmp(du_url, url) == 0) {
            SAH_TRACEZ_ERROR(ME, "URL's match, so we throw ERR_DUPLICATE_DEPLOYMENT_UNIT error: %d", ERR_DUPLICATE_DEPLOYMENT_UNIT);
            amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_DUPLICATE_DEPLOYMENT_UNIT);
            amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_DUPLICATE_DEPLOYMENT_UNIT_STR);

            dustatechange_event_error(uuid, operation, ERR_DUPLICATE_DEPLOYMENT_UNIT, ERR_DUPLICATE_DEPLOYMENT_UNIT_STR, oldexecutionenvref);
            status = amxd_status_invalid_arg;
            goto exitfree;
        } else {
            SAH_TRACEZ_ERROR(ME, "URL's don't match, so we throw ERR_DUPLICATE_DEPLOYMENT_UNIT_MULTIPLE_VERSION: %d", ERR_DUPLICATE_DEPLOYMENT_UNIT_MULTIPLE_VERSION);
            // TODO: Introduce compile switch to turn this error off
            amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_DUPLICATE_DEPLOYMENT_UNIT_MULTIPLE_VERSION);
            amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_DUPLICATE_DEPLOYMENT_UNIT_MULTIPLE_VERSION_STR);

            dustatechange_event_error(uuid, operation, ERR_DUPLICATE_DEPLOYMENT_UNIT_MULTIPLE_VERSION, ERR_DUPLICATE_DEPLOYMENT_UNIT_MULTIPLE_VERSION_STR, oldexecutionenvref);
            status = amxd_status_invalid_arg;
            goto exitfree;
        }
    }

    // check if EE is disabled
    if(check_ee_disabled(executionenvref, uuid, operation, args, oldexecutionenvref)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }
    amxd_function_defer(func, &callid, ret, NULL, NULL);
    amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_CALLID, callid);

    retb = container_eu_validateinstallargs(args);
    when_false(retb, exitdeferred);
    status = amxd_status_deferred;
    goto exitfree;
exitdeferred:
    amxd_function_deferred_remove(callid);
    amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_USP_INTERNAL_ERROR);
    // This can be that the module (packager) is not loaded or,
    // the package is not happy with the arguments provided (and thus returns)
    // or the object is missing in this call
    amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_USP_INTERNAL_ERROR_STR);
    dustatechange_event_error(uuid, operation, ERR_USP_INTERNAL_ERROR, ERR_USP_INTERNAL_ERROR_STR, oldexecutionenvref ? oldexecutionenvref : executionenvref);
exitfree:
    free(executionenvref);
    return status;
}

// TODO: rewrite to resemble _DeploymentUnit_Uninstall
amxd_status_t _DeploymentUnit_Update(amxd_object_t* object,
                                     UNUSED amxd_function_t* func,
                                     UNUSED amxc_var_t* args,
                                     amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, "Password");
    amxd_dm_t* dm = timingila_get_dm();
    bool retb = false;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_status_t helpstatus = amxd_status_unknown_error;
    const char* uuid = NULL;
    const char* oldexecutionenvref = NULL;
    const char* version = NULL;
    const char* operation = SM_DM_EVENT_DUSTATECHANGE_OPERATION_UPDATE;
    const char* duid = NULL;
    char* executionenvref = NULL;
    uint64_t callid;
    amxc_var_t params;

    amxc_var_init(&params);
    when_null(object, exiterror);

    amxd_object_get_params(object, &params, amxd_dm_access_public);

    uuid = GET_CHAR(&params, SM_DM_DU_UUID);
    oldexecutionenvref = GET_CHAR(&params, SM_DM_DU_EE_REF);
    executionenvref = strdup(oldexecutionenvref);
    version = GET_CHAR(&params, SM_DM_DU_VERSION);
    duid = GET_CHAR(&params, SM_DM_DU_DUID);

    if(check_uuid(uuid, operation, args, executionenvref)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }

    if(check_executionenvref(executionenvref, uuid, operation, args)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }


    helpstatus = handle_executionenvref_path(&executionenvref, args, operation, dm, uuid);
    if(helpstatus != amxd_status_ok) {
        status = helpstatus;
        goto exitfree;
    }

    // check if EE is disabled
    if(check_ee_disabled(executionenvref, uuid, operation, args, oldexecutionenvref)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }

    amxc_var_add_key(cstring_t, args, SM_DM_DU_UUID, uuid);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_EE_REF, executionenvref);
    // Current version -> new version to be deduced from url
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VERSION, version);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, duid);
    if(GET_ARG(args, SM_DM_DU_URL) == NULL) {
        const char* url = GET_CHAR(&params, SM_DM_DU_URL);
        if(url) {
            SAH_TRACEZ_WARNING(ME, "No " SM_DM_DU_URL " included, lets include the one from the dm: '%s'", url);
            amxc_var_add_key(cstring_t, args, SM_DM_DU_URL, url);
        } else {
            SAH_TRACEZ_ERROR(ME, "No " SM_DM_DU_URL " included, and cannot find it in the dm");
        }
    }

    amxd_function_defer(func, &callid, ret, NULL, NULL);
    amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_CALLID, callid);

    retb = container_eu_validateupdateargs(args);
    when_false(retb, exitdeferred);

    status = amxd_status_deferred;
    goto exitfree;
exitdeferred:
    amxd_function_deferred_remove(callid);
exiterror:
    amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_USP_INTERNAL_ERROR);
    // This can be that the module (packager) is not loaded or,
    // the package is not happy with the arguments provided (and thus returns)
    // or the object is missing in this call
    amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_USP_INTERNAL_ERROR_STR);
    dustatechange_event_error(uuid, operation, ERR_USP_INTERNAL_ERROR, ERR_USP_INTERNAL_ERROR_STR, oldexecutionenvref ? oldexecutionenvref : executionenvref);
exitfree:
    amxc_var_clean(&params);
    free(executionenvref);
    return status;
}

amxd_status_t _DeploymentUnit_Uninstall(amxd_object_t* object,
                                        UNUSED amxd_function_t* func,
                                        UNUSED amxc_var_t* args,
                                        amxc_var_t* ret) {
    lcm_dump_rpc(object, func, args, NULL);
    amxd_dm_t* dm = timingila_get_dm();
    bool retb = false;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_status_t helpstatus = amxd_status_unknown_error;
    bool retaindata = GET_BOOL(args, SM_DM_DU_RPC_RETAINDATA);
    const char* uuid = NULL;
    const char* oldexecutionenvref = NULL;
    const char* operation = SM_DM_EVENT_DUSTATECHANGE_OPERATION_UNINSTALL;
    char* executionenvref = NULL;
    amxc_var_t params;
    uint64_t callid;

    amxc_var_init(&params);
    when_null(object, exiterror);

    amxd_object_get_params(object, &params, amxd_dm_access_public);

    uuid = GET_CHAR(&params, SM_DM_DU_UUID);
    oldexecutionenvref = GET_CHAR(&params, SM_DM_DU_EE_REF);
    executionenvref = strdup(oldexecutionenvref);

    if(check_uuid(uuid, operation, args, executionenvref)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }

    if(check_executionenvref(executionenvref, uuid, operation, args)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }

    helpstatus = handle_executionenvref_path(&executionenvref, args, operation, dm, uuid);
    if(helpstatus != amxd_status_ok) {
        status = helpstatus;
        goto exitfree;
    }

#ifdef TIMINGILA_DEBUG
    const char* name = GET_CHAR(&params, SM_DM_DU_NAME);
    const char* vendor = GET_CHAR(&params, SM_DM_DU_VENDOR);
    const char* version = GET_CHAR(&params, SM_DM_DU_VERSION);
    const char* duid = GET_CHAR(&params, SM_DM_DU_DUID);

    SAH_TRACEZ_ERROR(ME, SM_DM_DU_RPC_RETAINDATA ": %d", retaindata);

    SAH_TRACEZ_INFO(ME, SM_DM_DU_UUID ": %s", uuid);
    SAH_TRACEZ_INFO(ME, SM_DM_DU_NAME ": %s", name);
    SAH_TRACEZ_INFO(ME, SM_DM_DU_VENDOR ": %s", vendor);
    SAH_TRACEZ_INFO(ME, SM_DM_DU_VERSION ": %s", version);
    SAH_TRACEZ_INFO(ME, SM_DM_DU_EE_REF ": %s", executionenvref);
    SAH_TRACEZ_INFO(ME, SM_DM_DU_DUID ": %s", duid);
#endif // TIMINGILA_DEBUG

    // check if EE is disabled
    if(check_ee_disabled(executionenvref, uuid, operation, args, oldexecutionenvref)) {
        status = amxd_status_invalid_arg;
        goto exitfree;
    }

    amxd_function_defer(func, &callid, ret, NULL, NULL);
    amxc_var_add_key(uint64_t, &params, AMXB_DEFERRED_CALLID, callid);
    amxc_var_add_key(bool, &params, SM_DM_DU_RPC_RETAINDATA, retaindata);

    retb = container_UninstallDu(&params);
    when_false(retb, exitdeferred);

    status = amxd_status_deferred;
    goto exitfree;
exitdeferred:
    amxd_function_deferred_remove(callid);
exiterror:
    amxc_var_add_key(uint64_t, args, AMXB_DEFERRED_ERR_CODE, ERR_USP_INTERNAL_ERROR);
    // This can be that the module (packager) is not loaded or,
    // the package is not happy with the arguments provided (and thus returns)
    // or the object is missing in this call
    amxc_var_add_key(cstring_t, args, AMXB_DEFERRED_ERR_MSG, ERR_USP_INTERNAL_ERROR_STR);
    dustatechange_event_error(uuid, operation, ERR_USP_INTERNAL_ERROR, ERR_USP_INTERNAL_ERROR_STR, oldexecutionenvref ? oldexecutionenvref : executionenvref);
exitfree:
    amxc_var_clean(&params);
    free(executionenvref);
    return status;
}
