/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"

#include "dmmngr_priv.h"
#include "timingila_priv.h"

#define ME TIMINGILA_MOD_DM_MNGR "-ee"

static int timingila_dm_mngr_update_ee(UNUSED const char* function_name,
                                       amxc_var_t* args,
                                       amxc_var_t* ret) {
    int retval = -1;
    amxd_object_t* dm_obj = NULL;
    amxd_dm_t* dm = timingila_get_dm();

    dm_obj = get_ee_obj_from_keys(dm, args);
    if(dm_obj) {
        // update instance
        SAH_TRACEZ_INFO(ME, "Update Exec Env instance");
        retval = timingila_dm_mngr_update(dm_obj, args);
    } else {
        // add new instance
        amxd_object_t* exec_unit = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EE);
        if(exec_unit == NULL) {
            SAH_TRACEZ_ERROR(ME, "Cannot get" SOFTWAREMODULES_DM_EE);
            goto exit;
        }

        SAH_TRACEZ_INFO(ME, "Create Exec Env instance");
        retval = timingila_dm_mngr_create(exec_unit, args, ret);
    }

exit:
    return retval;
}

static int timingila_dm_mngr_remove_ee(UNUSED const char* function_name,
                                       amxc_var_t* args,
                                       UNUSED amxc_var_t* ret) {
    int retval = -1;
    amxd_object_t* dm_obj = NULL;
    amxd_dm_t* dm = timingila_get_dm();

    const char* name = GET_CHAR(args, SM_DM_EE_NAME);
    const char* alias = GET_CHAR(args, SM_DM_EE_ALIAS);

    dm_obj = get_ee_obj_from_keys(dm, args);
    if(dm_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot find Exec Env instance [%s][%s]", name, alias);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Remove Exec Env instance [%s][%s]", name, alias);
    retval = timingila_dm_mngr_remove(&dm_obj);
exit:
    return retval;
}

static int timingila_dm_mngr_update_nc(UNUSED const char* function_name,
                                       amxc_var_t* args,
                                       UNUSED amxc_var_t* ret) {
    int retval = -1;
    amxd_object_t* dm_obj = NULL;
    amxd_dm_t* dm = timingila_get_dm();

    dm_obj = amxd_dm_findf(dm, SOFTWAREMODULES_DM_NC);
    if(dm_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot find " SOFTWAREMODULES_DM_NC);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Update " SOFTWAREMODULES_DM_NC);
    retval = timingila_dm_mngr_update(dm_obj, args);
exit:
    return retval;
}

int timingila_dm_mngr_ee_init(void) {
    timingila_dm_mngr_add_function(TIMINGILA_MOD_DM_MNGR_UPDATE_EE, timingila_dm_mngr_update_ee);
    timingila_dm_mngr_add_function(TIMINGILA_MOD_DM_MNGR_REMOVE_EE, timingila_dm_mngr_remove_ee);
    timingila_dm_mngr_add_function(TIMINGILA_MOD_DM_MNGR_UPDATE_NC, timingila_dm_mngr_update_nc);
    return 0;
}
