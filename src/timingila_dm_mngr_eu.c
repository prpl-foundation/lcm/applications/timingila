/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"

#include "dmmngr_priv.h"
#include "timingila_priv.h"

#define ME TIMINGILA_MOD_DM_MNGR "-eu"

static inline amxd_object_t* get_obj_from_keys(amxd_dm_t* dm, amxc_var_t* args) {
    amxd_object_t* ret = NULL;
    const char* euid = NULL;

    euid = GET_CHAR(args, SM_DM_EU_EUID);
    if(euid) {
        ret = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EU ".[" SM_DM_EU_EUID " == '%s'].", euid);
    } else {
        SAH_TRACEZ_ERROR(ME, "Cannot get " SM_DM_EU_EUID);
    }

    return ret;
}

typedef enum {
    EU_UNKNOWN = 0,
    EU_INSERT,
    EU_REMOVE
} EUAction_t;

#define MAX_STR_LEN_U32 12

static inline void eu_update_du_eulist(amxd_object_t* dm_obj_eu, amxc_var_t* args, EUAction_t action) {
    amxc_var_t retval;
    amxc_string_t eu_path;
    uint32_t index_eu = amxd_object_get_index(dm_obj_eu);
    amxd_dm_t* dm = timingila_get_dm();
    amxc_var_init(&retval);
    amxc_var_set_type(&retval, AMXC_VAR_ID_LIST);
    const char* eu_path_string;
    bool rethelp;

    if(container_eu_get_du_list(args, &retval) == false) {
        SAH_TRACEZ_ERROR(ME, "Cannot get du list from the eu");
        goto exit;
    }

    amxc_string_init(&eu_path, sizeof(SOFTWAREMODULES_DM_EU ".") + MAX_STR_LEN_U32);
    rethelp = amxc_string_setf(&eu_path, DEVICE_DM_PREFIX SOFTWAREMODULES_DM_EU ".%" PRIu32 ".", index_eu);
    eu_path_string = amxc_string_get(&eu_path, 0);
    if(rethelp) {
        SAH_TRACEZ_ERROR(ME, "The eu_path is probably wrong: '%s'", eu_path_string);
    }

    if(amxc_var_get_first(&retval) == NULL) {
        SAH_TRACEZ_ERROR(ME, "The list returned by the container adapter is empty");
    }

    amxc_var_for_each(data, &retval) {
        const char* duid = amxc_var_constcast(cstring_t, data);
        if((duid) && (duid[0] != '\0')) {
            amxd_object_t* du_obj = amxd_dm_findf(dm, SOFTWAREMODULES_DM_DU ".[" SM_DM_DU_DUID " == '%s'].", duid);
            if(du_obj) {
                amxd_trans_t transaction;
                const amxc_var_t* du_eulist_ro = amxd_object_get_param_value(du_obj, SM_DM_DU_EULIST);
                amxc_var_t du_eulist;
                amxc_var_init(&du_eulist);
                amxc_var_copy(&du_eulist, du_eulist_ro);
                amxc_var_cast(&du_eulist, AMXC_VAR_ID_LIST);
                switch(action) {
                case EU_INSERT:
                    SAH_TRACEZ_INFO(ME, "Adding EU ('%s') to EUlist of DU with DUID '%s'", eu_path_string, duid);
                    amxc_var_add(cstring_t, &du_eulist, eu_path_string);
                    break;
                case EU_REMOVE:
                    SAH_TRACEZ_INFO(ME, "Trying to remove EU ('%s') from EUlist of DU with DUID '%s'", eu_path_string, duid);
                    amxc_var_for_each(eu_entry, &du_eulist) {
                        const char* entry_eu_path = amxc_var_constcast(cstring_t, eu_entry);
                        if((entry_eu_path == NULL) || (entry_eu_path[0] == '\0')) {
                            SAH_TRACEZ_WARNING(ME, "Empty entry_eu_path: %p", entry_eu_path);
                        } else if(strncmp((eu_path_string + sizeof(SOFTWAREMODULES_DM_DU ".")), (entry_eu_path + sizeof(SOFTWAREMODULES_DM_DU ".")), MAX_STR_LEN_U32) == 0) {
                            SAH_TRACEZ_INFO(ME, "Found EU in EUlist!");
                            amxc_var_take_it(eu_entry);
                            amxc_var_clean(eu_entry);
                            amxc_var_delete(&eu_entry);
                        }
                    }
                    break;
                default:
                    SAH_TRACEZ_ERROR(ME, "Cannot parse action");
                    break;
                }
                // set in datamodel
                amxd_trans_init(&transaction);
                amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
                amxd_trans_select_object(&transaction, du_obj);
                amxd_trans_set_param(&transaction, SM_DM_DU_EULIST, &du_eulist);
                amxd_status_t status = amxd_trans_apply(&transaction, dm);
                switch(status) {
                case amxd_status_ok:
                    SAH_TRACEZ_INFO(ME, "Transaction succesfull");
                    break;
                case amxd_status_invalid_path:
                    SAH_TRACEZ_ERROR(ME, "amxd_status_invalid_path");
                    break;
                case amxd_status_missing_key:
                    SAH_TRACEZ_ERROR(ME, "amxd_status_missing_key");
                    break;
                case amxd_status_invalid_value:
                    SAH_TRACEZ_ERROR(ME, "amxd_status_invalid_value");
                    break;
                default:
                    SAH_TRACEZ_ERROR(ME, "amxd_trans_apply() ==> %d", status);
                    break;
                }
                amxd_trans_clean(&transaction);
                amxc_var_clean(&du_eulist);
            } else {
                SAH_TRACEZ_ERROR(ME, "Cannot find the DeploymentUnit with DUID: '%s'", duid);
            }
        } else {
            SAH_TRACEZ_ERROR(ME, "No duid found: %p", duid);
        }
    }

    amxc_string_clean(&eu_path);
exit:
    amxc_var_clean(&retval);
    return;
}

static int timingila_dm_mngr_update_eu(UNUSED const char* function_name,
                                       amxc_var_t* args,
                                       amxc_var_t* ret) {
    int retval = -1;
    amxd_object_t* dm_obj = NULL;
    amxd_dm_t* dm = timingila_get_dm();

    // Get Unique Keys
    dm_obj = get_obj_from_keys(dm, args);
    if(dm_obj) {
        // update instance
        SAH_TRACEZ_INFO(ME, "Update Exec Unit instance");
        retval = timingila_dm_mngr_update(dm_obj, args);
    } else {
        // add new instance
        amxd_object_t* exec_unit = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EU);
        if(exec_unit == NULL) {
            SAH_TRACEZ_ERROR(ME, "Cannot get" SOFTWAREMODULES_DM_EU);
            goto exit;
        }

        SAH_TRACEZ_INFO(ME, "Create Exec Unit instance");
        retval = timingila_dm_mngr_create(exec_unit, args, ret);
        dm_obj = get_obj_from_keys(dm, args);
        if(dm_obj) {
            eu_update_du_eulist(dm_obj, args, EU_INSERT);
        } else {
            SAH_TRACEZ_ERROR(ME, "Cannot find newly addd EU?");
        }
    }

exit:
    return retval;
}

static int timingila_dm_mngr_remove_eu(UNUSED const char* function_name,
                                       amxc_var_t* args,
                                       UNUSED amxc_var_t* ret) {
    int retval = -1;
    amxd_object_t* dm_obj = NULL;
    amxd_dm_t* dm = timingila_get_dm();

    // Get Unique Keys
    dm_obj = get_obj_from_keys(dm, args);
    if(dm_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot find Exec Unit instance");
        goto exit;
    }

    eu_update_du_eulist(dm_obj, args, EU_REMOVE);

    SAH_TRACEZ_INFO(ME, "Remove Exec Unit instance");
    retval = timingila_dm_mngr_remove(&dm_obj);
exit:
    return retval;
}


int timingila_dm_mngr_eu_init(void) {
    timingila_dm_mngr_add_function(TIMINGILA_MOD_DM_MNGR_UPDATE_EU, timingila_dm_mngr_update_eu);
    timingila_dm_mngr_add_function(TIMINGILA_MOD_DM_MNGR_REMOVE_EU, timingila_dm_mngr_remove_eu);
    return 0;
}
