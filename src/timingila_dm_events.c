/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"

#include "dmmngr_priv.h"
#include "timingila_priv.h"
#include "dmevents_priv.h"

#define ME TIMINGILA_MOD_DM_MNGR "-events"

static amxm_module_t* mod_dm_events = NULL;

void timingila_event(amxd_object_t* object,
                     const char* name,
                     amxc_var_t* const data) {

    when_null_log(name, exit);
    when_null_log(data, exit);

    if(object == NULL) {
        SAH_TRACEZ_WARNING(ME, "No object supplied, using " TIMINGILA_DM " root");
        amxd_dm_t* dm = timingila_get_dm();
        object = amxd_dm_get_object(dm, TIMINGILA_DM);
    }

    amxd_object_emit_signal(object, name, data);
exit:
    return;
}

void softwaremodules_event(amxd_object_t* object,
                           const char* name,
                           amxc_var_t* const data) {
    ASSERT_NOT_NULL(object, return , "Event %s won't be sent: Event object missing", name);
    timingila_event(object, name, data);
}

int timingila_dm_events_add_function(const char* func_name, amxm_callback_t cb) {
    int retval = -1;

    when_str_empty_log(func_name, exit);
    when_null_log(cb, exit);

    SAH_TRACEZ_INFO(ME, "Adding function: %s, %s, %s", "self", TIMINGILA_MOD_DM_EVENT, func_name);
    retval = amxm_module_add_function(mod_dm_events, func_name, cb);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldn't add function: '%s'", func_name);
    }
exit:
    return retval;
}

int timingila_dm_events_init(void) {
    int retval = -1;
    amxm_shared_object_t* so = amxm_get_so(AMXM_SELF);
    if(so == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot get " AMXM_SELF);
        goto exit;
    }

    if(amxm_module_register(&mod_dm_events, so, TIMINGILA_MOD_DM_EVENT)) {
        SAH_TRACEZ_ERROR(ME, "Cannot register module");
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}
