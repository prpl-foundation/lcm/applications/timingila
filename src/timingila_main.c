/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"
#include "timingila/timingila_signals.h"

#include "timingila_priv.h"
#include "timingila/timingila_module.h"

static timingila_app_t app;

amxo_parser_t* timingila_get_parser(void) {
    return app.parser;
}

amxd_dm_t* timingila_get_dm(void) {
    return app.dm;
}

static void late_init_timer_cb(amxp_timer_t* timer, UNUSED void* priv) {
    SAH_TRACE_WARNING(EVENT_TIMINGILA_LATE_INIT_START);
    timingila_emit_signal(EVENT_TIMINGILA_LATE_INIT_START, NULL);
    amxp_timer_delete(&timer);
    return;
}


static void timingila_start_late_init_timer(void) {
    amxp_timer_t* timer = NULL;
    int rc = amxp_timer_new(&timer, late_init_timer_cb, NULL);
    if(rc) {
        SAH_TRACE_ERROR("Could't init timer");
        return;
    }

    rc = amxp_timer_start(timer, 5000);
    if(rc) {
        SAH_TRACE_ERROR("Couldn't start timer");
        return;
    }

    SAH_TRACE_WARNING("Started late init timer");
    return;
}

int _timingila_main(int reason,
                    amxd_dm_t* dm,
                    amxo_parser_t* parser) {
    switch(reason) {
    case 0: // start
        app.dm = dm;
        app.parser = parser;
        // Init dm-mngr
        SAH_TRACE_INFO(TIMINGILA " init " TIMINGILA_MOD_DM_MNGR);
        timingila_dm_mngr_init();
        timingila_dm_mngr_du_init();
        timingila_dm_mngr_eu_init();
        timingila_dm_mngr_ee_init();

        SAH_TRACE_INFO(TIMINGILA " init " TIMINGILA_MOD_DM_EVENT);
        timingila_dm_events_init();
        timingila_dm_events_softwaremodules_init();

        // Init generic signals
        SAH_TRACE_INFO(TIMINGILA " init module/internal events");
        timingila_init_generic_events();

        // Init timingila
        SAH_TRACE_INFO(TIMINGILA " init plugin infrastructure");
        timingila_init_plugins();
        SAH_TRACE_INFO(TIMINGILA " service started");
        timingila_start_late_init_timer();
#ifdef TIMINGILA_INIT_ADAPTERS
#if TIMINGILA_INIT_ADAPTERS
        // TODO: make this configureable with Timingila.config
        SAH_TRACE_INFO(TIMINGILA " auto init adapters on startup");
#pragma message("TIMINGILA_PATH_SO_CONTAINER: " TIMINGILA_PATH_SO_CONTAINER)
        open_container_adapter(TIMINGILA_PATH_SO_CONTAINER);
#pragma message("TIMINGILA_PATH_SO_PACKAGER: " TIMINGILA_PATH_SO_PACKAGER)
        open_packager_adapter(TIMINGILA_PATH_SO_PACKAGER);
#endif // TIMINGILA_INIT_ADAPTERS=1
#endif // TIMINGILA_INIT_ADAPTERS
        break;
    case 1: // stop
        // Cleanup timingila
        SAH_TRACE_INFO(TIMINGILA " cleanup plugins");
        timingila_cleanup_plugins();
        SAH_TRACE_INFO(TIMINGILA " cleanup generic events");
        timingila_cleanup_generic_events();
        app.dm = NULL;
        app.parser = NULL;
        SAH_TRACE_INFO(TIMINGILA " service stopped");
        break;
    default:
        SAH_TRACE_ERROR(TIMINGILA " has to do what?");
        break;
    }

    return 0;
}
