/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"

#include "softwaremodules_priv.h"
#include "timingila_priv.h"
#include "dmevents_priv.h"

#define ME "softwaremodules_events"

// TODO:
// 1. handle starttime and completetime
//
int timingila_dm_mngr_event_dustatechange(UNUSED const char* function_name,
                                          amxc_var_t* args,
                                          UNUSED amxc_var_t* ret) {
    int retval = -1;
    amxd_object_t* dm_obj = NULL;
    amxd_dm_t* dm = timingila_get_dm();
    char* uuid = NULL;
    char* version = NULL;
    const char* currentstatus = NULL;
    const char* operationperformed = NULL;
    uint32_t faultcode = 0;
    bool resolved = true;
    amxc_var_t* notif_data = NULL;
    amxc_var_t* vardata = NULL;
    char emptystr[1] = {0};
    const char* faultstring = NULL;
    amxd_object_t* eu_obj = NULL;
    char* euref = NULL;
    char* duid = NULL;
    char* eeref = NULL;
    amxd_object_t* sm_obj = NULL;

#ifdef TIMINGILA_DEBUG
    amxc_var_dump(args, 1);
#endif // TIMINGILA_DEBUG

    // TODO: define format of input for dustatechangeevent

    version = amxc_var_dyncast(cstring_t, GET_ARG(args, SM_DM_EVENT_DUSTATECHANGE_VERSION));

    // Get du dm (provide uuid and/or alias)
    dm_obj = get_du_obj_from_keys(dm, args);
    if(dm_obj) {
        SAH_TRACEZ_INFO(ME, "Found DU in the DM! Using values found in the datamodel");
        eeref = amxd_object_get_cstring_t(dm_obj, SM_DM_DU_EE_REF, NULL);
        uuid = amxd_object_get_cstring_t(dm_obj, SM_DM_DU_UUID, NULL);
        duid = amxd_object_get_cstring_t(dm_obj, SM_DM_DU_DUID, NULL);
        if(version == NULL) {
            version = amxd_object_get_cstring_t(dm_obj, SM_DM_DU_VERSION, NULL);
        }
    }
    //take the values from the args, if they are not defined in the dm
    if(string_is_empty(eeref)) {
        free(eeref);
        eeref = amxc_var_dyncast(cstring_t, GET_ARG(args, SM_DM_EVENT_DUSTATECHANGE_EE_REF));
        if(!eeref) {
            SAH_TRACEZ_ERROR(ME, "Cannot get " SM_DM_EVENT_DUSTATECHANGE_EE_REF);
            eeref = strdup(emptystr);
        }
    }
    if(string_is_empty(uuid)) {
        free(uuid);
        uuid = amxc_var_dyncast(cstring_t, GET_ARG(args, SM_DM_EVENT_DUSTATECHANGE_UUID));
        if(!uuid) {
            SAH_TRACEZ_ERROR(ME, "Cannot get " SM_DM_EVENT_DUSTATECHANGE_UUID);
            uuid = strdup(emptystr);
        }
    }
    if(string_is_empty(duid)) {
        free(duid);
        duid = amxc_var_dyncast(cstring_t, GET_ARG(args, SM_DM_DU_DUID));
        if(!duid) {
            SAH_TRACEZ_ERROR(ME, "Cannot get " SM_DM_DU_DUID);
            duid = strdup(emptystr);
        }
    }

    if(eeref == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot get " SM_DM_EVENT_DUSTATECHANGE_EE_REF);
        eeref = strdup(emptystr);
    }

    // Get eu dm (provide euid)
    eu_obj = amxd_dm_findf(dm, SOFTWAREMODULES_DM_EU ".[" SM_DM_EU_EUID " == '%s'].", duid);
    if(eu_obj) {
        euref = get_object_devices_path(eu_obj, AMXD_OBJECT_INDEXED);
    }

    currentstatus = GET_CHAR(args, SM_DM_EVENT_DUSTATECHANGE_CURRENTSTATE);
    if(currentstatus == NULL) {
        currentstatus = GET_CHAR(args, SM_DM_DU_STATUS);
        if(currentstatus == NULL) {
            SAH_TRACEZ_WARNING(ME, "Cannot get " SM_DM_EVENT_DUSTATECHANGE_CURRENTSTATE "/" SM_DM_DU_STATUS);
            currentstatus = emptystr;
        }
    }

    if(GET_ARG(args, SM_DM_EVENT_DUSTATECHANGE_RESOLVED) != NULL) {
        resolved = GET_BOOL(args, SM_DM_EVENT_DUSTATECHANGE_RESOLVED);
    } else {
        SAH_TRACEZ_WARNING(ME, "Cannot get " SM_DM_EVENT_DUSTATECHANGE_RESOLVED);
    }

    operationperformed = GET_CHAR(args, SM_DM_EVENT_DUSTATECHANGE_OPERATION);
    if(operationperformed == NULL) {
        SAH_TRACEZ_WARNING(ME, "Cannot get " SM_DM_EVENT_DUSTATECHANGE_OPERATION);
        operationperformed = emptystr;
    }

    if(GET_ARG(args, SM_DM_EVENT_DUSTATECHANGE_FAULTCODE)) {
        faultcode = GET_UINT32(args, SM_DM_EVENT_DUSTATECHANGE_FAULTCODE);
    } else {
        SAH_TRACEZ_WARNING(ME, "Cannot get " SM_DM_EVENT_DUSTATECHANGE_FAULTCODE);
    }

    faultstring = GET_CHAR(args, SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING);
    if(faultstring == NULL) {
        SAH_TRACEZ_WARNING(ME, "Cannot get " SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING);
        faultstring = emptystr;
    }

    amxc_var_new(&notif_data);

    amxc_var_set_type(notif_data, AMXC_VAR_ID_HTABLE);
    vardata = amxc_var_add_key(amxc_htable_t, notif_data, "data", NULL);
    amxc_var_add_key(cstring_t, vardata, SM_DM_EVENT_DUSTATECHANGE_UUID, uuid);
    amxc_var_add_key(cstring_t, vardata, SM_DM_EVENT_DUSTATECHANGE_VERSION, version);
    amxc_var_add_key(cstring_t, vardata, SM_DM_EVENT_DUSTATECHANGE_CURRENTSTATE, currentstatus);
    amxc_var_add_key(bool, vardata, SM_DM_EVENT_DUSTATECHANGE_RESOLVED, resolved);
    amxc_var_add_key(cstring_t, vardata, SM_DM_EVENT_DUSTATECHANGE_OPERATION, operationperformed);
    amxc_var_add_key(cstring_t, vardata, SM_DM_EVENT_DUSTATECHANGE_EE_REF, eeref);
    amxc_var_add_key(cstring_t, vardata, SM_DM_EVENT_DUSTATECHANGE_EUREFLIST, euref ? euref : emptystr);
    amxc_var_add_key(uint32_t, vardata, SM_DM_EVENT_DUSTATECHANGE_FAULT "." SM_DM_EVENT_DUSTATECHANGE_FAULTCODE, faultcode);
    amxc_var_add_key(cstring_t, vardata, SM_DM_EVENT_DUSTATECHANGE_FAULT "." SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING, faultstring);

    // DUSTATECHANGE is to be sent on SoftwareModules root object
    sm_obj = amxd_dm_get_object(dm, SOFTWAREMODULES_DM);
    softwaremodules_event(sm_obj, SM_DM_EVENT_DUSTATECHANGE, notif_data);

    amxc_var_delete(&notif_data);
    free(uuid);
    free(version);
    free(eeref);
    free(euref);
    free(duid);

    return retval;
}


int timingila_dm_events_softwaremodules_init(void) {
    timingila_dm_events_add_function(TIMINGILA_MOD_DM_EVENT_DUSTATECHANGE, timingila_dm_mngr_event_dustatechange);
    return 0;
}
