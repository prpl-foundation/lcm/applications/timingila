/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"

#include "timingila_priv.h"
#include "timingila_container_adapter.h"

#define ME "container_adapter"

static amxm_shared_object_t* container_adapter = NULL;

static bool container_adapter_init(void) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_t* var_dm = amxc_var_add_new_key(&args, TIMINGILA_CONTAINER_INIT_DM);
    amxc_var_set_type(var_dm, AMXC_VAR_ID_ANY);
    var_dm->data.data = (void*) timingila_get_dm();

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_INIT,
                                     &args,
                                     &ret);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    return error ? false : true;
}

amxm_shared_object_t* get_container_adapter(void) {
    return container_adapter;
}

bool open_container_adapter(const char* plugin_so) {
    bool ret = false;

    if(container_adapter) {
        SAH_TRACEZ_ERROR(ME, "Container adapter already set: %p", container_adapter);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Load plugin (%s) as container", plugin_so);
    if(amxm_so_open(&container_adapter, "container", plugin_so) != 0) {
        SAH_TRACEZ_ERROR(ME, "Can not load plugin %s", plugin_so);
        goto exit;
    }

    if(amxm_so_get_module(container_adapter, MOD_TIMINGILA_CONTAINER) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Library does not provide %s", MOD_TIMINGILA_CONTAINER);
        amxm_so_close(&container_adapter);
        goto exit;
    }

    {
        amxd_dm_t* dm = timingila_get_dm();
        amxd_object_t* timingila = amxd_dm_get_object(dm, TIMINGILA_DM);
        amxc_var_t dmchange;
        amxc_var_init(&dmchange);
        amxc_var_set_type(&dmchange, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &dmchange, "ContainerPluginPath", plugin_so);
        timingila_dm_mngr_update(timingila, &dmchange);
        amxc_var_clean(&dmchange);
    }

    if(!container_adapter_init()) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize the container adapter");
        goto exit;
    }

    ret = true;
exit:
    return ret;
}

bool close_container_adapter(void) {
    return amxm_so_close(&container_adapter);
}

bool container_UninstallDu(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_UNINSTALLDU,
                                     args,
                                     &ret);
    amxc_var_clean(&ret);

    return error ? false : true;
}

bool container_eu_setrequestedstate(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EU_SETREQUESTEDSTATE,
                                     args,
                                     &ret);

    return error ? false : true;
}

bool container_eu_setautostart(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EU_SETAUTOSTART,
                                     args,
                                     &ret);

    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_eu_modifynetworkconfig(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EU_MODIFYNETWORKCONFIG,
                                     args,
                                     &ret);

    return error ? false : true;
}

bool container_eu_validateinstallargs(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EU_VALIDATEINSTALLARGS,
                                     args,
                                     &ret);
    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_eu_validateupdateargs(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EU_VALIDATEUPDATEARGS,
                                     args,
                                     &ret);
    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_ee_setenable(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_SETENABLE,
                                     args,
                                     &ret);

    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_ee_add(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_ADD,
                                     args,
                                     &ret);

    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_ee_restart(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_RESTART,
                                     args,
                                     &ret);

    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_ee_modifyconstraints(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_MODIFYCONSTRAINTS,
                                     args,
                                     &ret);

    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_ee_modifyavailableroles(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_MODIFYAVAILABLEROLES,
                                     args,
                                     &ret);

    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_ee_delete(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_DELETE,
                                     args,
                                     &ret);

    amxc_var_clean(&ret);
    return error ? false : true;
}

bool container_ee_stats(amxc_var_t* args, amxc_var_t* retval) {
    int error = 0;

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_STATS,
                                     args,
                                     retval);

    return error ? false : true;
}

bool container_ee_description(amxc_var_t* args) {
    int error = 0;
    amxc_var_t ret;
    amxc_var_init(&ret);

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_DESCRIPTION,
                                     args,
                                     &ret);
    amxc_var_clean(&ret);

    return error ? false : true;
}

bool container_eu_stats(amxc_var_t* args, amxc_var_t* retval) {
    int error = 0;

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EU_STATS,
                                     args,
                                     retval);

    return error ? false : true;
}

bool container_eu_uptime(amxc_var_t* args, amxc_var_t* retval) {
    int error = 0;

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EU_UPTIME,
                                     args,
                                     retval);

    return error ? false : true;
}

// Returns a list of DUID's based on the EUID
bool container_eu_get_du_list(amxc_var_t* args, amxc_var_t* retval) {
    int error = 0;

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EU_GET_CORRESPONDING_DUS,
                                     args,
                                     retval);

    return error ? false : true;
}

bool container_ee_read_param(amxc_var_t* args, amxc_var_t* retval) {
    int error = 0;

    error = amxm_so_execute_function(container_adapter,
                                     MOD_TIMINGILA_CONTAINER,
                                     TIMINGILA_CONTAINER_EE_READPARAM,
                                     args,
                                     retval);

    return error ? false : true;
}
