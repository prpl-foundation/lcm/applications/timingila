/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <debug/sahtrace.h>

#include "timingila/timingila_defines.h"
#include "timingila/timingila_signals.h"

#include "timingila_priv.h"
#include "timingila/timingila_module.h"

#define ME "event"

static int timingila_add_event(const char* signal_name) {
    int retval = -1;

    if((signal_name == NULL) || (signal_name[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "signal_name is empty");
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Add event: %s", signal_name);

    retval = timingila_create_signal(signal_name);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldnt add %s [%d]", signal_name, retval);
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

void timingila_init_generic_events(void) {
    timingila_add_event(EVENT_TIMINGILA_LATE_INIT_START);
    timingila_add_event(EVENT_TIMINGILA_LATE_INIT_HANDOVER);
    timingila_add_event(EVENT_TIMINGILA_LATE_INIT_DONE);

    timingila_add_event(EVENT_PACKAGER_INSTALL_DU_DONE);
    timingila_add_event(EVENT_PACKAGER_INSTALL_DU_FAILED);

    timingila_add_event(EVENT_PACKAGER_UNINSTALL_DU_DONE);
    timingila_add_event(EVENT_PACKAGER_UNINSTALL_DU_FAILED);

    timingila_add_event(EVENT_PACKAGER_UPDATE_DU_FAILED);
    timingila_add_event(EVENT_PACKAGER_UPDATE_DU_DONE);

    timingila_add_event(EVENT_CONTAINER_INSTALL_DU_DONE);
    timingila_add_event(EVENT_CONTAINER_INSTALL_DU_FAILED);

    timingila_add_event(EVENT_CONTAINER_UNINSTALL_DU_DONE);
    timingila_add_event(EVENT_CONTAINER_UNINSTALL_DU_FAILED);

    timingila_add_event(EVENT_CONTAINER_UPDATE_DU_FAILED);
    timingila_add_event(EVENT_CONTAINER_UPDATE_DU_DONE);

    timingila_add_event(EVENT_CONTAINER_ADD_EE_FAILED);
    timingila_add_event(EVENT_CONTAINER_ADD_EE_DONE);

    timingila_add_event(EVENT_CONTAINER_DELETE_EE_FAILED);
    timingila_add_event(EVENT_CONTAINER_DELETE_EE_DONE);

    timingila_add_event(EVENT_CONTAINER_VALIDATE_INSTALL_DU_DONE);
    timingila_add_event(EVENT_CONTAINER_VALIDATE_UPDATE_DU_DONE);

    timingila_add_event(EVENT_EXEC_PACKAGER_UNINSTALL_DU);
}

void timingila_cleanup_generic_events(void) {

}
