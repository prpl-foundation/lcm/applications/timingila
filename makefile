include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C doc clean

install: all
	$(INSTALL) -D -p -m 0644 ./odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 ./odl/$(COMPONENT)_defaults.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults.odl
	$(INSTALL) -D -p -m 0644 ./odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 ./odl/softwaremodules_defaults.odl $(DEST)/etc/amx/$(COMPONENT)/softwaremodules_defaults.odl
	$(INSTALL) -D -p -m 0644 ./odl/softwaremodules_definition.odl $(DEST)/etc/amx/$(COMPONENT)/softwaremodules_definition.odl
	$(INSTALL) -D -p -m 0644 ./odl/softwaremodules_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-softwaremodules_mapping.odl
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)$(LIBDIR)/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/timingila
	$(INSTALL) -D -p -m 0644 ./include/timingila/*.h $(DEST)$(INCLUDEDIR)/timingila/
	$(INSTALL) -d -m 0755 $(DEST)/usr/bin
	ln -sfr $(DEST)/usr/bin/amxrt $(DEST)/usr/bin/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/timingila-core.sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0644 ./odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 ./odl/$(COMPONENT)_defaults.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults.odl
	$(INSTALL) -D -p -m 0644 ./odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 ./odl/softwaremodules_defaults.odl $(PKGDIR)/etc/amx/$(COMPONENT)/softwaremodules_defaults.odl
	$(INSTALL) -D -p -m 0644 ./odl/softwaremodules_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/softwaremodules_definition.odl
	$(INSTALL) -D -p -m 0644 ./odl/softwaremodules_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-softwaremodules_mapping.odl
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)$(LIBDIR)/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/timingila
	$(INSTALL) -D -p -m 0644 ./include/timingila/*.h $(PKGDIR)$(INCLUDEDIR)/timingila/
	$(INSTALL) -d -m 0755 $(PKGDIR)/usr/bin
	rm -f $(PKGDIR)/usr/bin/$(COMPONENT)
	ln -sfr $(PKGDIR)/usr/bin/amxrt $(PKGDIR)/usr/bin/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/timingila-core.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc

	$(eval ODLFILES += ./odl/$(COMPONENT).odl)
	$(eval ODLFILES += ./odl/$(COMPONENT)_defaults.odl)
	$(eval ODLFILES += ./odl/$(COMPONENT)_definition.odl)
	$(eval ODLFILES += ./odl/softwaremodules_defaults.odl)
	$(eval ODLFILES += ./odl/softwaremodules_definition.odl)
	$(eval ODLFILES += ./odl/softwaremodules_mapping.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test