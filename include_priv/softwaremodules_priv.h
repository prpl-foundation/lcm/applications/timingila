/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__SOFTWAREMODULES_PRIV_H__)
#define __SOFTWAREMODULES_PRIV_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>

#include <amxm/amxm.h>

#include "timingila_common.h"

// Deployment Units
/**
   @ingroup SoftwareModules
   @defgroup DeploymentUnits
 # Deployment Units
 ## Sequence Diagram
 ### installDu
   \startuml
   !include ../../../doc/resources/plantuml/long_sequence_diagram_installdu.puml
   \enduml

 */

/**
   @ingroup DeploymentUnits
   @brief
   Starts installing a DU, this will call the packageradapter's installDu function

   @return
   amxd_status_unknown_error if an error occured. amxd_status_ok (0) on success. In reality it will almost always return amxd_status_ok so the return value is very limited in reporting success of the full chain.
 */
amxd_status_t _SoftwareModules_InstallDU(amxd_object_t* softwaremodules,
                                         amxd_function_t* func,
                                         amxc_var_t* args,
                                         amxc_var_t* ret);

amxd_status_t _SoftwareModules_AddExecEnv(amxd_object_t* object,
                                          amxd_function_t* func,
                                          amxc_var_t* args,
                                          amxc_var_t* ret);

amxd_status_t _DeploymentUnit_Update(amxd_object_t* object,
                                     amxd_function_t* func,
                                     amxc_var_t* args,
                                     amxc_var_t* ret);

amxd_status_t _DeploymentUnit_Uninstall(amxd_object_t* object,
                                        amxd_function_t* func,
                                        amxc_var_t* args,
                                        amxc_var_t* ret);

amxd_status_t _ExecEnv_SetRunLevel(amxd_object_t* object,
                                   amxd_function_t* func,
                                   amxc_var_t* args,
                                   amxc_var_t* ret);

amxd_status_t _ExecEnv_Restart(amxd_object_t* object,
                               amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret);

amxd_status_t _ExecEnv_ModifyConstraints(amxd_object_t* object,
                                         amxd_function_t* func,
                                         amxc_var_t* args,
                                         amxc_var_t* ret);

amxd_status_t _ExecEnv_ModifyAvailableRoles(amxd_object_t* object,
                                            amxd_function_t* func,
                                            amxc_var_t* args,
                                            amxc_var_t* ret);

amxd_status_t _ExecEnv_Delete(amxd_object_t* object,
                              amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret);

amxd_status_t _ee_read_stats(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

amxd_status_t _eu_read_stats(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

amxd_status_t _eu_read_uptime(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);

amxd_status_t _ee_read_param(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

amxd_status_t _ExecutionUnit_SetRequestedState(amxd_object_t* object,
                                               amxd_function_t* func,
                                               amxc_var_t* args,
                                               amxc_var_t* ret);

amxd_status_t _ExecutionUnit_ModifyNetworkConfig(amxd_object_t* object,
                                                 amxd_function_t* func,
                                                 amxc_var_t* args,
                                                 amxc_var_t* ret);

void _eu_change_autostart(const char* const sig_name,
                          const amxc_var_t* const data,
                          void* const priv);

void _ee_change_enable(const char* const sig_name,
                       const amxc_var_t* const data,
                       void* const priv);

amxd_status_t _ee_write_description(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv);

amxd_status_t check_uuid(const char* uuid, const char* operation, amxc_var_t* args, const char* executionenvref);

#ifdef __cplusplus
}
#endif

#endif // __SOFTWAREMODULES_PRIV_H__
