/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__TIMINGILA_CONTAINER_ADAPTER_H__)
#define __TIMINGILA_CONTAINER_ADAPTER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include "timingila_common.h"

amxm_shared_object_t* get_container_adapter(void);
bool open_container_adapter(const char* plugin_so);
bool close_container_adapter(void);
bool container_UninstallDu(amxc_var_t* args);
bool container_eu_setrequestedstate(amxc_var_t* args);
bool container_eu_setautostart(amxc_var_t* args);
bool container_eu_modifynetworkconfig(amxc_var_t* args);
bool container_eu_validateinstallargs(amxc_var_t* args);
bool container_eu_validateupdateargs(amxc_var_t* args);
bool container_ee_setenable(amxc_var_t* args);
bool container_ee_add(amxc_var_t* args);
bool container_ee_restart(amxc_var_t* args);
bool container_ee_modifyconstraints(amxc_var_t* args);
bool container_ee_delete(amxc_var_t* args);
bool container_ee_stats(amxc_var_t* args, amxc_var_t* retval);
bool container_eu_stats(amxc_var_t* args, amxc_var_t* retval);
bool container_ee_description(amxc_var_t* args);
bool container_eu_get_du_list(amxc_var_t* args, amxc_var_t* retval);
bool container_ee_modifyavailableroles(amxc_var_t* args);
bool container_eu_uptime(amxc_var_t* args, amxc_var_t* retval);
bool container_ee_read_param(amxc_var_t* args, amxc_var_t* retval);

#ifdef __cplusplus
}
#endif

#endif // __TIMINGILA_CONTAINER_ADAPTER_H__
