# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.0.0 - 2024-10-02(08:43:42 +0000)

### Other

- - [amx] Failing to restart processes with init scripts
- Implement unprivileged containers
- Cthulhu API validation

## Release v2.3.1 - 2024-07-18(09:30:16 +0000)

### Other

- Align datamodel definition with TR-181

## Release v2.3.0 - 2024-07-17(13:14:33 +0000)

### Other

- Create USP Controller instance when a container added

## Release v2.2.4 - 2024-07-16(13:15:29 +0000)

### Other

- DUStateChange! event sent out for wrong object

## Release v2.2.3 - 2024-05-30(13:47:06 +0000)

### Other

- fix memory leaks

## Release v2.2.2 - 2024-05-16(14:04:08 +0000)

## Release v2.2.1 - 2024-05-06(14:03:01 +0000)

### Other

- do not print password in the logs

## Release v2.2.0 - 2024-02-09(13:52:40 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.1.1 - 2024-01-12(13:55:35 +0000)

### Other

- disable dmproxy code since it might cause crashes

## Release v2.1.0 - 2023-12-15(13:18:52 +0000)

### Other

- Service discovery - Advertisement via DNS.SD

## Release v2.0.2 - 2023-10-04(19:28:59 +0000)

### Other

- Implement ControllerVersion parameter

## Release v2.0.1 - 2023-10-04(12:43:12 +0000)

### Other

- Opensource component
- - Update license

## Release v2.0.0 - 2023-09-11(14:12:47 +0000)

### Other

- EE ModifyConstraints not aligned with standard

## Release v1.8.0 - 2023-09-08(07:04:43 +0000)

### Other

- define capabilities needed by process

## Release v1.7.2 - 2023-09-07(11:48:15 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v1.7.1 - 2023-08-25(09:34:34 +0000)

### Other

- [Prpl] Init scripts do not have a shutdown target - LCM Team Components

## Release v1.7.0 - 2023-07-28(07:57:06 +0000)

### Other

- SoftwareModules.ExecutionUnit.{i}.HostObject is missing in datamodel

## Release v1.6.0 - 2023-07-12(13:58:37 +0000)

### Other

- Add global NetworkConfig

## Release v1.5.3 - 2023-07-11(13:09:53 +0000)

### Other

- async syntax has changed

## Release v1.5.2 - 2023-06-29(09:53:09 +0000)

### Other

- Update NetworkConfig in datamodel

## Release v1.5.1 - 2023-06-23(09:24:09 +0000)

### Other

- Move signature from Annotations to installDU() parameter

## Release v1.5.0 - 2023-06-19(12:56:00 +0000)

### Other

- Add defines for NetworkConfig

## Release v1.4.0 - 2023-06-07(17:07:12 +0000)

### Other

- Handle eeref path in uninstalldu and updatedu

## Release v1.3.0 - 2023-06-06(14:11:59 +0000)

### Other

- Report error for installing multiple versions of the same du in the same ee

## Release v1.2.0 - 2023-06-02(15:26:56 +0000)

### Other

- InstallDU(): ExecutionEnvRef needs to be Path Name

## Release v1.1.0 - 2023-06-02(14:33:43 +0000)

### Other

- implement missing parameter SoftwareModules. DeploymentUnit.1.ExecutionUnitList

## Release v1.0.0 - 2023-06-02(13:40:32 +0000)

### Other

- DU/EU Unique Identifier

## Release v0.9.0 - 2023-04-19(13:44:59 +0000)

### Other

- the UUID should conform to the format defined in  RFC 4122 Version 5

## Release v0.8.1 - 2023-04-17(12:12:09 +0000)

### Other

- Wrong operation when Uninstall of a DU on a disabled EE

## Release v0.8.0 - 2023-04-03(08:28:31 +0000)

### Other

- Implement HostObjects

## Release v0.7.1 - 2023-03-14(18:51:37 +0000)

### Other

- [Cthulhu] upgrading container when not enough space on disk...
- [LCM][Shutdown] timingila and cthulhu stops script are called...

## Release v0.7.0 - 2023-03-10(15:12:59 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.6.8 - 2023-03-01(09:12:17 +0000)

### Other

- [LCM] Expose time for firstInstall and lastupdate

## Release v0.6.7 - 2023-02-27(10:13:30 +0000)

### Other

- [timingila] Use STAGING_LIBDIR instead of LIBDIR in src/makefile

## Release v0.6.6 - 2023-02-24(14:32:42 +0000)

### Other

- DU installation failure should be reported as such
- [softwaremodule] "Version is empty" fail is popping when updating DU

## Release v0.6.5 - 2023-02-13(13:39:19 +0000)

### Fixes

- Remove duplicated code

### Other

- during Update restrictions should be updatable

## Release v0.6.4 - 2023-02-10(13:07:50 +0000)

### Other

- [LCM] Error code is not forwarded correctly from LCM to USP

## Release v0.6.3 - 2023-02-07(13:38:02 +0000)

### Other

- [Timingila-rlyeh] Missing information on reboot

## Release v0.6.2 - 2023-01-18(13:18:55 +0000)

### Other

- print a uint64_t in a portable way

## Release v0.6.1 - 2023-01-16(18:08:33 +0000)

### Other

- Log RPC calls in a specific tracezone

## Release v0.6.0 - 2023-01-16(16:13:38 +0000)

### Fixes

- Change error code 7003 to 7002
- Change error code 7003 to 7002

## Release v0.5.1 - 2023-01-12(16:24:38 +0000)

### Other

- return 7226 error when InstallDU called with existing UUID

## Release v0.5.0 - 2023-01-10(10:19:12 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.4.7 - 2022-11-03(09:43:29 +0000)

## Release v0.4.6 - 2022-10-24(12:54:53 +0000)

### Fixes

- enable core dumps

### Other

- allow for several subdirs on repo
- crash when uninstalling a DU

## Release v0.4.5 - 2022-09-22(09:16:58 +0000)

## Release v0.4.4 - 2022-09-21(13:17:46 +0000)

## Release v0.4.3 - 2022-09-21(09:53:00 +0000)

### Other

- The EU and DU are not removed when calling Delete() on the EE, which was disabled prior to deletion

## Release v0.4.2 - 2022-09-08(12:07:18 +0000)

### Other

- add AutoStart parameter to InstallDU
- Unexpected removed DU after an Update

## Release v0.4.1 - 2022-07-05(12:44:53 +0000)

### Other

- After deleting the EE the DU and EU installed in it are still available

## Release v0.4.0 - 2022-07-04(12:58:44 +0000)

### New

- allow setting EU resources durings installation

### Other

- allow setting EU resources durings installation
- It is not possible to retrieve the Creation Timestamp related parameter for the EE, it does not exist

## Release v0.3.7 - 2022-06-03(09:49:16 +0000)

### Other

- [cthulhu] expose EU resource limitations in Cthulhu and Timingila

## Release v0.3.6 - 2022-05-18(09:52:27 +0000)

### Other

- : Execution Environments: Remove (and LCM-111, LCM-112)

## Release v0.3.5 - 2022-05-18(09:29:00 +0000)

### Other

- Deployment Units: Retrieve. Install Timestamp

## Release v0.3.4 - 2022-05-17(12:27:03 +0000)

### Other

- LCM-136 : Execution Environments: Add Execution Environments: Add. Notifications - Operation Complete

## Release v0.3.3 - 2022-05-02(12:05:25 +0000)

### Other

- Execution Environments: Modify. Description

## Release v0.3.2 - 2022-04-25(13:55:45 +0000)

### Other

- Expose Purge api to TR181 DM

## Release v0.3.1 - 2022-04-19(14:01:21 +0000)

### Other

- Add CI tests to Timingila

## Release v0.3.0 - 2022-03-28(12:33:39 +0000)

### New

- Read resources from EU

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.2.13 - 2022-03-20(20:50:04 +0000)

### Other

- InstallDU is performed while EE is disabled although it should not according to spec

## Release v0.2.12 - 2022-03-10(08:51:49 +0000)

### Other

- ModifyConstraints() value's type not check

## Release v0.2.11 - 2022-03-07(16:13:13 +0000)

### Other

- Command to modify resoiurce constraints of an Execution Environment

## Release v0.2.10 - 2022-02-22(19:56:37 +0000)

### Other

- [LCM] Expose in SoftwareModules the available mem, diskspace of an EE (sandbox)

## Release v0.2.9 - 2022-02-04(09:16:52 +0000)

### Other

- [LCM] Constrain the EE (sandbox) with mem, cpu, diskspace

## Release v0.2.8 - 2022-01-28(12:45:17 +0000)

### Other

- ExecutionUnit: Restart

## Release v0.2.7 - 2022-01-27(14:00:27 +0000)

### Other

- Execution Environments: Restart

## Release v0.2.6 - 2021-12-13(17:21:21 +0000)

## Release v0.2.5 - 2021-12-03(02:48:29 +0000)

### Other

- Implement DeploymentUnit Update

## Release v0.2.4 - 2021-11-23(21:00:40 +0000)

## Release v0.2.3 - 2021-11-23(12:56:26 +0000)

## Release v0.2.2 - 2021-10-15(13:15:54 +0000)

### Fixes

- Fix auto load preprocessor

## Release v0.2.1 - 2021-10-14(10:03:12 +0000)

## Release v0.2.0 - 2021-10-07(11:02:31 +0000)

### New

- [Timingila] Implement DU.Uninstall()

## Release v0.1.3 - 2021-09-22(13:37:31 +0000)

### Fixes

-  Auto init adapters

## Release v0.1.2 - 2021-09-14(11:02:59 +0000)

## Release v0.1.1 - 2021-09-14(08:35:04 +0000)

## Release v0.1.0 - 2021-09-09(13:32:26 +0000)

## Release v0.0.1 - 2021-09-09(10:41:44 +0100)

### New

- Initial release
