/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _TIMINGILA_DEFINES_H_
#define _TIMINGILA_DEFINES_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define TIMINGILA_X_PRPL "X_PRPL-COM_"

#define TIMINGILA "Timingila"
#define TIMINGILA_MOD_DM_MNGR_UPDATE_DU "upd-du"
#define TIMINGILA_MOD_DM_MNGR_REMOVE_DU "rm-du"
#define TIMINGILA_MOD_DM_MNGR_UPDATE_EU "upd-eu"
#define TIMINGILA_MOD_DM_MNGR_REMOVE_EU "rm-eu"
#define TIMINGILA_MOD_DM_MNGR_UPDATE_EE "upd-ee"
#define TIMINGILA_MOD_DM_MNGR_REMOVE_EE "rm-ee"
#define TIMINGILA_MOD_DM_MNGR_UPDATE_NC "upd-nc"
#define TIMINGILA_MOD_DM_MNGR "dm-mngr"

#define TIMINGILA_MOD_DM_EVENT_DUSTATECHANGE "dustatechange"
#define TIMINGILA_MOD_DM_EVENT "dm-event"

#define TIMINGILA_ARG_PLUGIN "plugin"
#define MOD_TIMINGILA_CONTAINER "mod_timingila_container"
#define MOD_TIMINGILA_PACKAGER "mod_timingila_packager"

#define TIMINGILA_DM TIMINGILA

#define SM_ALIAS "Alias"
#define SM_NAME "Name"

#define SM_DM_DU_UUID "UUID"
#define SM_DM_DU_DUID "DUID"
#define SM_DM_DU_URL "URL"
#define SM_DM_DU_USERNAME "Username"
#define SM_DM_DU_PASSWORD "Password"
#define SM_DM_DU_EE_REF "ExecutionEnvRef"
#define SM_DM_DU_ALLOCDISKSPACE "AllocatedDiskSpace"
#define SM_DM_DU_ALLOCMEM "AllocatedMemory"
#define SM_DM_DU_ALLOCCPUPERCENT "AllocatedCPUPercent"
#define SM_DM_DU_AUTOSTART "AutoStart"
#define SM_DM_DU_HOSTOBJECT "HostObject"
#define SM_DM_DU_NETWORKCONFIG "NetworkConfig"
#define SM_DM_DU_DATA "Data"
#define SM_DM_DU_STATUS "Status"
#define SM_DM_DU_VENDOR "Vendor"
#define SM_DM_DU_VERSION "Version"
#define SM_DM_DU_VERSION_OLD "OldVersion"
#define SM_DM_DU_DESCRIPTION "Description"
#define SM_DM_DU_INSTALLED "Installed"
#define SM_DM_DU_LASTUPDATE "LastUpdate"
#define SM_DM_DU_NAME SM_NAME
#define SM_DM_DU_ALIAS SM_ALIAS
#define SM_DM_DU_RPC_RETAINDATA "RetainData"
#define SM_DM_DU_DISKLOCATION "DiskLocation"
#define SM_DM_DU_EULIST "ExecutionUnitList"
#define SM_DM_DU_SIGNATURE "Signature"
#define SM_DM_DU_MODULEVERSION "ModuleVersion"
#define SM_DM_DU_ENVIRONMENTVARIABLES "EnvVariable"
#define SM_DM_DU_ENVIRONMENTVARIABLES_KEY "Key"
#define SM_DM_DU_ENVIRONMENTVARIABLES_VALUE "Value"
#define SM_DM_DU_REQUIRED_ROLES "RequiredRoles"
#define SM_DM_DU_OPTIONAL_ROLES "OptionalRoles"
#define SM_DM_DU_PRIVILEGED "Privileged"
#define SM_DM_DU_NUMREQUIREDUIDS "NumRequiredUIDs"
#define SM_DM_DU_APPLICATIONDATA "ApplicationData"

#define SM_DM_DU_STATUS_INSTALLING "Installing"
#define SM_DM_DU_STATUS_INSTALLED "Installed"
#define SM_DM_DU_STATUS_UPDATING "Updating"

#define SM_DM_DU_STATUS_UNINSTALLING "Uninstalling"
#define SM_DM_DU_STATUS_UNINSTALLED "Uninstalled"
#define SM_DM_DU_STATUS_FAILED "Failed"

#define DEVICE_DM "Device"
#define DEVICE_DM_PREFIX DEVICE_DM "."
#define SOFTWAREMODULES_DM "SoftwareModules"
#define SOFTWAREMODULES_DM_DU SOFTWAREMODULES_DM ".DeploymentUnit"

#define SM_DM_EU_STATUS_IDLE "Idle"
#define SM_DM_EU_STATUS_STARTING "Starting"
#define SM_DM_EU_STATUS_ACTIVE "Active"
#define SM_DM_EU_STATUS_STOPPING "Stopping"
#define SM_DM_EU_STATUS_RESTART "Restart"
#define SM_DM_EU_STATUS_RESTARTING "Restarting"

#define SM_DM_EU_EXEC_F_CODE_NOF "NoFault"
#define SM_DM_EU_EXEC_F_CODE_START_FO "FailureOnStart"
#define SM_DM_EU_EXEC_F_CODE_START_FOA "FailureOnAutoStart"
#define SM_DM_EU_EXEC_F_CODE_STOP_FO "FailureOnStop"
#define SM_DM_EU_EXEC_F_CODE_FWA "FailureWhileActive"
#define SM_DM_EU_EXEC_F_CODE_DF "DependencyFailure"
#define SM_DM_EU_EXEC_F_CODE_US "UnStartable"

#define SM_DM_EU_EUID "EUID"
#define SM_DM_EU_ALIAS SM_ALIAS
#define SM_DM_EU_NAME SM_NAME
#define SM_DM_EU_EE_LABEL "ExecEnvLabel"
#define SM_DM_EU_STATUS "Status"
#define SM_DM_EU_EXEC_F_CODE "ExecutionFaultCode"
#define SM_DM_EU_EXEC_F_MSG "ExecutionFaultMessage"
#define SM_DM_EU_AUTOSTART "AutoStart"
#define SM_DM_EU_RUNLEVEL "RunLevel"
#define SM_DM_EU_VENDOR SM_DM_DU_VENDOR
#define SM_DM_EU_VERSION SM_DM_DU_VERSION
#define SM_DM_EU_DESCRIPTION SM_DM_DU_DESCRIPTION
#define SM_DM_EU_ALLOCDISKSPACE SM_DM_DU_ALLOCDISKSPACE
#define SM_DM_EU_AVAILDISKSPACE "AvailableDiskSpace"
#define SM_DM_EU_ASSIGNED_ROLES "AssignedRoles"
#define SM_DM_EU_ALLOCMEM SM_DM_DU_ALLOCMEM
#define SM_DM_EU_AVAILMEM "AvailableMemory"
#define SM_DM_EU_ALLOCCPUPERCENT SM_DM_DU_ALLOCCPUPERCENT
#define SM_DM_EU_DISKSPACEINUSE "DiskSpaceInUse"
#define SM_DM_EU_MEMORYINUSE "MemoryInUse"
#define SM_DM_EU_REFERENCES "References"
#define SM_DM_EU_ASOC_PROC_LST "AssociatedProcessList"
#define SM_DM_EU_VENDOR_LOG_LST "VendorLogList"
#define SM_DM_EU_VENDOR_CONFIG_LST "VendorConfigList"
#define SM_DM_EU_EE_REF SM_DM_DU_EE_REF
#define SM_DM_EU_REQSTATE "RequestedState"
#define SM_DM_EU_NC "NetworkConfig"
#define SM_DM_EU_NC_ACCESSINTERFACES "AccessInterfaces"
#define SM_DM_EU_NC_ACCESSINTERFACES_REFERENCE "Reference"
#define SM_DM_EU_NC_PORTFORWARDING "PortForwarding"
#define SM_DM_EU_NC_PORTFORWARDING_INTERFACE "Interface"
#define SM_DM_EU_NC_PORTFORWARDING_EXTERNALPORT "ExternalPort"
#define SM_DM_EU_NC_PORTFORWARDING_INTERNALPORT "InternalPort"
#define SM_DM_EU_NC_PORTFORWARDING_PROTOCOL "Protocol"
#define SM_DM_EU_NC_DNSSDREFLIST TIMINGILA_X_PRPL "DNSSDRefList"
#define SM_DM_EU_HOSTOBJECT "HostObject"
#define SM_DM_EU_HOSTOBJECT_SOURCE "Source"
#define SM_DM_EU_HOSTOBJECT_DESTINATION "Destination"
#define SM_DM_EU_HOSTOBJECT_OPTIONS "Options"
#define SM_DM_EU_ENVIRONMENTVARIABLES "EnvVariable"
#define SM_DM_EU_ENVIRONMENTVARIABLES_KEY "Key"
#define SM_DM_EU_ENVIRONMENTVARIABLES_VALUE "Value"
#define SM_DM_EU_CREATED_TIME "CreationTime"
#define SM_DM_EU_UPTIME "Uptime"
#define SM_DM_EU_ALLOCATEDEUUID "AllocatedEUUID"
#define SM_DM_EU_ALLOCATEDHOSTUID "AllocatedHostUID"
#define SM_DM_EU_ALLOCATEDHOSTGID "AllocatedHostGID"
#define SM_DM_EU_ALLOCATEDEUGID "AllocatedEUGID"
#define SM_DM_EU_AUTORESTART "AutoRestart"
#define SM_DM_EU_AUTORESTART_ENABLE "Enable"
#define SM_DM_EU_AUTORESTART_MAX_RETRYCOUNT "MaximumRetryCount"
#define SM_DM_EU_AUTORESTART_MIN_INT "RetryMinimumWaitInterval"
#define SM_DM_EU_AUTORESTART_MAX_INT "RetryMaximumWaitInterval"
#define SM_DM_EU_AUTORESTART_RETRY_INT "RetryIntervalMultiplier"
#define SM_DM_EU_AUTORESTART_RESET "ResetPeriod"
#define SM_DM_EU_AUTORESTART_RETRY_COUNT "RetryCount"
#define SM_DM_EU_AUTORESTART_LASTRESTART "LastRestarted"
#define SM_DM_EU_AUTORESTART_NEXTRESTART "NextRestart"

#define SOFTWAREMODULES_DM_EU SOFTWAREMODULES_DM ".ExecutionUnit"

#define SOFTWAREMODULES_DM_NC SOFTWAREMODULES_DM ".NetworkConfig"
#define SM_DM_NC_INTERFACES "Interfaces"
#define SM_DM_NC_INTERFACES_NAME "Name"
#define SM_DM_NC_INTERFACES_REFERENCE "Reference"
#define SM_DM_NC_DEFAULTBRIDGE "DefaultBridge"
#define SM_DM_NC_DEFAULTFIREWALLCHAIN "DefaultFirewallChain"

#define SM_DM_EU_STATUS_IDLE "Idle"
#define SM_DM_EU_STATUS_STARTING "Starting"
#define SM_DM_EU_STATUS_ACTIVE "Active"
#define SM_DM_EU_STATUS_STOPPING "Stopping"
#define SM_DM_EU_STATUS_RESTARTING "Restarting"

#define SM_DM_EE_STATUS_DISABLED "Disabled"
#define SM_DM_EE_STATUS_UP "Up"
#define SM_DM_EE_STATUS_ERROR "Error"
#define SM_DM_EE_STATUS_RESTARTING SM_DM_EU_STATUS_RESTARTING

#define SM_DM_EE "ExecEnv"
#define SM_DM_EE_ENABLE "Enable"
#define SM_DM_EE_STATUS SM_DM_DU_STATUS
#define SM_DM_EE_ALIAS SM_ALIAS
#define SM_DM_EE_NAME SM_NAME
#define SM_DM_EE_TYPE "Type"
#define SM_DM_EE_INITRUNLEVEL "InitialRunLevel"
#define SM_DM_EE_CURRUNLEVEL "CurrentRunLevel"
#define SM_DM_EE_INITEXECRUNLEVEL "InitialExecutionUnitRunLevel"
#define SM_DM_EE_VENDOR SM_DM_DU_VENDOR
#define SM_DM_EE_VERSION SM_DM_DU_VERSION
#define SM_DM_EE_ALLOCDISKSPACE SM_DM_DU_ALLOCDISKSPACE
#define SM_DM_EE_AVAILDISKSPACE SM_DM_EU_AVAILDISKSPACE
#define SM_DM_EE_ALLOCMEM SM_DM_DU_ALLOCMEM
#define SM_DM_EE_AVAILMEM SM_DM_EU_AVAILMEM
#define SM_DM_EE_ALLOCCPUPERCENT SM_DM_DU_ALLOCCPUPERCENT
#define SM_DM_EE_ACTIVE_EUS "ActiveExecutionUnits"
#define SM_DM_EE_PROC_REF_LST "ProcessorRefList"
#define SM_DM_EE_PARENT_EE "ParentExecEnv"
#define SM_DM_EE_AVAILABLE_ROLES "AvailableRoles"
#define SM_DM_EE_RESTART_REASON "RestartReason"
#define SM_DM_EE_DESCRIPTION "Description"
#define SM_DM_EE_CREATEDAT "CreatedAt"
#define SM_DM_EE_ARG_FORCE "Force"
#define SM_DM_EE_APPLICATIONDATA "ApplicationData"
#define SM_DM_EE_APPLICATIONDATA_NAME "Name"
#define SM_DM_EE_APPLICATIONDATA_CAPACITY "Capacity"
#define SM_DM_EE_APPLICATIONDATA_ENCRYPTED "Encrypted"
#define SM_DM_EE_APPLICATIONDATA_RETAIN "Retain"
#define SM_DM_EE_APPLICATIONDATA_ACCESSPATH "AccessPath"
#define SM_DM_EE_APPLICATIONDATA_ALIAS "Alias"
#define SM_DM_EE_APPLICATIONDATA_APPLICATIONUUID "ApplicationUUID"
#define SM_DM_EE_APPLICATIONDATA_UTILIZATION "Utilization"

#define SOFTWAREMODULES_DM_EE SOFTWAREMODULES_DM "." SM_DM_EE

#define SM_EE_STAT_NAME "StatName"
#define SM_EU_STAT_NAME SM_EE_STAT_NAME

#define SM_DM_EU_NAME SM_NAME
#define SM_DM_EU_ALIAS SM_ALIAS

#define SM_EE_RELPATH "RelPath"
#define SM_EE_PARAM "Param"

#define SOFTWAREMODULES_DM_EU SOFTWAREMODULES_DM ".ExecutionUnit"

#define TIMINGILA_PACKAGER_INSTALLDU "InstallDu"
#define TIMINGILA_PACKAGER_UNINSTALLDU "UninstallDu"
#define TIMINGILA_PACKAGER_UPDATEDU "UpdateDu"
#define TIMINGILA_PACKAGER_RMAFTERUNINSTALL "RmAfterUninstall"

#define TIMINGILA_CONTAINER_INSTALLDU TIMINGILA_PACKAGER_INSTALLDU
#define TIMINGILA_CONTAINER_UNINSTALLDU TIMINGILA_PACKAGER_UNINSTALLDU
#define TIMINGILA_CONTAINER_EU_SETREQUESTEDSTATE "SetRequestedState"
#define TIMINGILA_CONTAINER_EU_SETAUTOSTART "EUSetAutoStart"
#define TIMINGILA_CONTAINER_EU_MODIFYNETWORKCONFIG "ModifyNetworkConfig"
#define TIMINGILA_CONTAINER_EU_STATS "EUStats"
#define TIMINGILA_CONTAINER_EU_UPTIME "EUUptime"
#define TIMINGILA_CONTAINER_EU_VALIDATEINSTALLARGS "ValidateInstallArgs"
#define TIMINGILA_CONTAINER_EU_VALIDATEUPDATEARGS "ValidateUpdateArgs"

#define TIMINGILA_CONTAINER_EE_SETENABLE "EESetEnable"
#define TIMINGILA_CONTAINER_EE_ADD "EEAdd"
#define TIMINGILA_CONTAINER_EE_RESTART "EERestart"
#define TIMINGILA_CONTAINER_EE_MODIFYCONSTRAINTS "EEModifyConstraints"
#define TIMINGILA_CONTAINER_EE_DELETE "EEDelete"
#define TIMINGILA_CONTAINER_EE_STATS "EEStats"
#define TIMINGILA_CONTAINER_EE_DESCRIPTION "EEDescription"
#define TIMINGILA_CONTAINER_EE_MODIFYAVAILABLEROLES "EEModifyAvailableRoles"
#define TIMINGILA_CONTAINER_EE_READPARAM "EEReadParam"

#define TIMINGILA_CONTAINER_DU_GET_EU_LIST "DUGetEUList"
#define TIMINGILA_CONTAINER_EU_GET_CORRESPONDING_DUS "EUGetDUs"

#define TIMINGILA_CONTAINER_INIT "Initialize"
#define TIMINGILA_CONTAINER_INIT_DM "DataModel"

#define TM_DM_PROXYMANAGER TIMINGILA_DM ".ProxyManager"
#define TM_DM_PROXYMANAGER_REGISTER "register"
#define TM_DM_PROXYMANAGER_UNREGISTER "unregister"
#define TM_DM_PROXYMANAGER_ARG_PROXY "proxy"
#define TM_DM_PROXYMANAGER_ARG_REAL "real"

#define SM_DM_EVENT_DUSTATECHANGE_UUID SM_DM_DU_UUID
#define SM_DM_EVENT_DUSTATECHANGE_VERSION SM_DM_DU_VERSION
#define SM_DM_EVENT_DUSTATECHANGE_EE_REF SM_DM_DU_EE_REF
#define SM_DM_EVENT_DUSTATECHANGE_CURRENTSTATE "CurrentState"
#define SM_DM_EVENT_DUSTATECHANGE_RESOLVED "Resolved"
#define SM_DM_EVENT_DUSTATECHANGE_EUREFLIST "ExecutionUnitRefList"
#define SM_DM_EVENT_DUSTATECHANGE_STARTTIME "StartTime"
#define SM_DM_EVENT_DUSTATECHANGE_COMPLETETIME "CompleteTime"
#define SM_DM_EVENT_DUSTATECHANGE_OPERATION "OperationPerformed"
#define SM_DM_EVENT_DUSTATECHANGE_FAULTCODE "FaultCode"
#define SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING "FaultString"
#define SM_DM_EVENT_DUSTATECHANGE_FAULT "Fault"
#define SM_DM_EVENT_DUSTATECHANGE_DEPLOYMENTUNITREF "DeploymentUnitRef"

#define SM_DM_EVENT_DUSTATECHANGE_INSTALLED SM_DM_DU_STATUS_INSTALLED
#define SM_DM_EVENT_DUSTATECHANGE_UNINSTALLED SM_DM_DU_STATUS_UNINSTALLED
#define SM_DM_EVENT_DUSTATECHANGE_FAILED SM_DM_DU_STATUS_FAILED

#define SM_DM_EVENT_DUSTATECHANGE_OPERATION_INSTALL "Install"
#define SM_DM_EVENT_DUSTATECHANGE_OPERATION_UPDATE "Update"
#define SM_DM_EVENT_DUSTATECHANGE_OPERATION_UNINSTALL "Uninstall"

#define MODULE_ARGS_DM_OBJ "dmobj"
#define MODULE_ARGS_FUNCTION "arguments"

#define SM_DM_EVENT_DUSTATECHANGE "DUStateChange!"

#define AMXM_NOTIFICATION "notification"
#define AMXB_APP_START "app:start"
#define AMXB_APP_STOP "app:stop"
#define AMXM_SELF "self"
#define AMXB_DEFERRED_CALLID "AmxbDefferedCallId"
#define AMXB_DEFERRED_ERR_CODE "err_code"
#define AMXB_DEFERRED_ERR_MSG "err_msg"
#define AMXB_WAIT_DONE "wait:done"

#define ERR_USP_INTERNAL_ERROR_STR "Request Denied"
#define ERR_USP_INTERNAL_ERROR 7002
#define ERR_INVALID_COMMAND_ARGUMENTS_STR "Invalid Command Arguments"
#define ERR_INVALID_COMMAND_ARGUMENTS 7027
#define ERR_INVALID_DEPLOYMENT_UNIT_STATE_STR "Invalid Deployment Unit State"
#define ERR_INVALID_DEPLOYMENT_UNIT_STATE 7229
#define ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH_STR "Deployment Unit to Execution Environment Mismatch"
#define ERR_DEPLOYMENT_UNIT_TO_EXECUTION_ENVIRONMENT_MISMATCH 7225
#define ERR_DUPLICATE_DEPLOYMENT_UNIT_STR "Duplicate Deployment Unit"
#define ERR_DUPLICATE_DEPLOYMENT_UNIT 7226
#define ERR_DUPLICATE_DEPLOYMENT_UNIT_MULTIPLE_VERSION_STR "Multiple version of the same DU of the same EE is not supported"
#define ERR_DUPLICATE_DEPLOYMENT_UNIT_MULTIPLE_VERSION ERR_USP_INTERNAL_ERROR
#define ERR_UNKNOWN_EXECUTION_ENVIRONMENT_STR "Unknown Execution Environment"
#define ERR_UNKNOWN_EXECUTION_ENVIRONMENT 7223
#define ERR_MISSING_EXECUTION_ENVIRONMENT_STR ERR_UNKNOWN_EXECUTION_ENVIRONMENT_STR ": Empty or missing " SM_DM_DU_EE_REF
#define ERR_MISSING_EXECUTION_ENVIRONMENT ERR_UNKNOWN_EXECUTION_ENVIRONMENT
#define ERR_UUID_FORMAT_NON_COMPLIANT_STR ERR_INVALID_COMMAND_ARGUMENTS_STR ": UUID does not match the format specified in RFC 4122 Version 5"
#define ERR_UUID_FORMAT_NON_COMPLIANT ERR_USP_INTERNAL_ERROR

#ifdef __cplusplus
}
#endif

#endif // _TIMINGILA_DEFINES_H_
