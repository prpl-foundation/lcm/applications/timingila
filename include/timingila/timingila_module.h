/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _TIMINGILA_MODULE_H_
#define _TIMINGILA_MODULE_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdlib.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_function.h>
#include <lcm/lcm_assert.h>
#include <timingila/timingila_defines.h>

// Timingila eventing

static inline int timingila_add_slot_to_signal(const char* const sig_name,
                                               const char* const expression,
                                               amxp_slot_fn_t fn,
                                               void* const priv) {
    return amxp_slot_connect(NULL, sig_name, expression, fn, priv);
}

static inline int timingila_remove_slot_from_signal(const char* const sig_name,
                                                    amxp_slot_fn_t fn) {
    return amxp_slot_disconnect(NULL, sig_name, fn);
}

static inline int timingila_emit_signal(const char* const sig_name,
                                        const amxc_var_t* const data) {
    return amxp_sigmngr_emit_signal(NULL, sig_name, data);
}

static inline int timingila_create_signal(const char* const sig_name) {
    amxp_signal_t* sig = NULL;
    return amxp_signal_new(NULL, &sig, sig_name);
}

static inline int timingila_dm_mngr_execute(const char* const func_name, amxc_var_t* args, amxc_var_t* ret) {
    return amxm_execute_function(AMXM_SELF, TIMINGILA_MOD_DM_MNGR, func_name, args, ret);
}

static inline int timingila_dm_event_execute(const char* const func_name, amxc_var_t* args, amxc_var_t* ret) {
    return amxm_execute_function(AMXM_SELF, TIMINGILA_MOD_DM_EVENT, func_name, args, ret);
}

// ---

// Timingila command llist

typedef unsigned int timingila_command_id_type_t;
typedef amxc_llist_t timingila_command_llist_type;

typedef struct timingila_command_entry {
    timingila_command_id_type_t command_id;
    amxc_var_t* priv;
    amxc_llist_it_t it;
} timingila_command_entry_t;

static inline int timingila_command_llist_init(timingila_command_llist_type* cmdlst) {
    return amxc_llist_init(cmdlst);
}

static inline int timingila_command_add(timingila_command_llist_type* const cmdlst, timingila_command_entry_t* cmd) {
    return amxc_llist_append(cmdlst, &(cmd->it));
}

static inline int timingila_command_add_new(timingila_command_llist_type* cmdlst,
                                            timingila_command_id_type_t command_id,
                                            void* priv) {
    int retval = -255;

    timingila_command_entry_t* command = (timingila_command_entry_t*) calloc(1, sizeof(timingila_command_entry_t));
    when_null(command, exit);

    command->command_id = command_id;
    command->priv = (amxc_var_t*) priv;

    retval = timingila_command_add(cmdlst, command);
    if(retval) {
        free(command);
    }
exit:
    return retval;
}

static inline timingila_command_entry_t* timingila_command_find(timingila_command_llist_type* cmdlst, timingila_command_id_type_t command_id) {
    timingila_command_entry_t* ret = NULL;
    when_null(cmdlst, exit);

    amxc_llist_for_each(it, cmdlst) {
        ret = amxc_llist_it_get_data(it, timingila_command_entry_t, it);
        timingila_command_id_type_t current_command_id = ret->command_id;
        if(current_command_id == command_id) {
            break;
        }
        ret = NULL;
    }

exit:
    return ret;
}

static inline void timingila_command_free(timingila_command_entry_t* cmd) {
    when_null(cmd, exit);

    if(cmd->priv) {
        amxc_var_delete(&cmd->priv);
    }

    free(cmd);
exit:
    return;
}

static inline int timingila_command_remove(timingila_command_entry_t** cmd) {
    int ret = -255;
    timingila_command_entry_t* derefcmd = *cmd;

    when_null(cmd, exit);
    when_null(derefcmd, exit);

    amxc_llist_it_take(&(derefcmd->it));
    timingila_command_free(derefcmd);

    ret = 0;
exit:
    return ret;
}

static void timingila_command_llist_delete_fn(amxc_llist_it_t* it) {
    timingila_command_entry_t* cmd = amxc_container_of(it, timingila_command_entry_t, it);
    timingila_command_free(cmd);
    return;
}

static inline void timingila_command_llist_cleanup(timingila_command_llist_type* cmdlst) {
    amxc_llist_clean(cmdlst, timingila_command_llist_delete_fn);
    return;
}

static inline timingila_command_id_type_t timingila_command_id_from_hex_str(const char* cmd_id_str) {
    return strtol(cmd_id_str, NULL, 16);
}

#define SIZE_COMMAND_ID_STR 17

static inline int timingila_command_id_to_hex_str(char* dst, timingila_command_id_type_t command_id) {
    return snprintf(dst, SIZE_COMMAND_ID_STR, "%X", command_id);
}

// ---

// Timingila unsigned int counter random init

#define DEV_RANDOM "/dev/urandom"

static inline timingila_command_id_type_t timingila_init_command_id_counter(void) {
    unsigned int seed = 0;
    size_t bytes = 0;

    FILE* urandom = fopen(DEV_RANDOM, "r");
    if(urandom == NULL) {
        goto exit;
    }

    do {
        bytes += fread(&seed, 1, sizeof(timingila_command_id_type_t) - bytes, urandom);
    } while(bytes < sizeof(timingila_command_id_type_t));

    fclose(urandom);
exit:
    srand(seed);
    return rand();
}

// ---
#ifndef GET_UINT64
#define GET_UINT64(a, n) amxc_var_dyncast(uint64_t, n == NULL ? a : GET_ARG(a, n))
#endif

#ifdef __cplusplus
}
#endif

#endif // _TIMINGILA_MODULE_H_
