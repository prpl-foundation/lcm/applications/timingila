# Timingila

[[_TOC_]]

## Introduction

Timingila is the embedded LCM agent that implements the Broadband Forum [TR-181](https://cwmp-data-models.broadband-forum.org/tr-181-2-14-1-cwmp.html) (namely the [SoftwareModules](https://cwmp-data-models.broadband-forum.org/tr-181-2-14-1-cwmp.html#D.Device:2.Device.SoftwareModules.)) and translates it to the respective underlying technology. Some refer to it as the DSM-core.

## Adapters
Timingila does not have any specific knowledge on how the underlying techonology works, and is just the controller for the datamodel. To translate the datamodel to the underlying technology Timingila uses adapters (Shared Objects), see [AMXM](https://gitlab.com/soft.at.home/ambiorix/libraries/libamxm) for more information. 

The modules themselves will be synchronically called from the datamodel directly, or they will use signal-slots (see [AMXP](https://gitlab.com/soft.at.home/ambiorix/libraries/libamxp) for more information) to construct callback chains. For instance the installDU call will be an aggregation of the packager adapter (who fetches the container) and the container adapter (that will effectively run and manage the lifecycle of the container).
